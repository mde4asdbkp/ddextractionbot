package nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.any
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Context
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.mock
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentMatchers.anyString
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.`when`
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import java.util.*

/**
 * Acceptance tests for the HttpNaturalLanguageProcessor
 *
 * Test functions intended to be self documenting.
 */
class HttpNaturalLanguageProcessorTest {

    private val mockRestTemplate: RestTemplate = mock()

    private val sut: HttpNaturalLanguageProcessor = HttpNaturalLanguageProcessor(
            port = "0",
            baseUrl = "baseUrl",
            restTemplate = mockRestTemplate
    )

    companion object {

        private const val TEST_MESSAGE = "test_message"

    }

    // Tests for processAddMessage

    @Test
    fun `should throw UnparsableMessageException if 4xx recieved from POST test`() {
        //Given
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(DesignDecision::class.java))).then { throw HttpClientErrorException(HttpStatus.BAD_REQUEST) }
        //When, Then
        assertThrows<UnparsableMessageException> {
            sut.processAddMessage(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `should throw Exception if 5xx recieved from POST test`() {
        //Given
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(DesignDecision::class.java))).then { throw HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR) }
        //When, Then
        assertThrows<Exception> {
            sut.processAddMessage(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `should throw Exception if null response body from POST test`() {
        //Given
        val mockResponse = mock<ResponseEntity<DesignDecision>>()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.OK)
        `when`(mockResponse.body).thenReturn(null)
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(DesignDecision::class.java))).thenReturn(mockResponse)
        //When, Then
        assertThrows<Exception> {
            sut.processAddMessage(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `should throw Exception if non OK status code from POST test`() {
        //Given
        val mockResponse = mock<ResponseEntity<DesignDecision>>()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED)
        `when`(mockResponse.body).thenReturn(mock())
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(DesignDecision::class.java))).thenReturn(mockResponse)
        //When, Then
        assertThrows<Exception> {
            sut.processAddMessage(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `should return response body from POST if not null test`() {
        //Given
        val mockResponse: ResponseEntity<DesignDecision> = mock()
        val mockDesignDecision: DesignDecision = mock()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.OK)
        `when`(mockResponse.body).thenReturn(mockDesignDecision)
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(DesignDecision::class.java))).thenReturn(mockResponse)
        //When
        val response = sut.processAddMessage(TEST_MESSAGE, UUID.randomUUID())
        //Then
        assertEquals(mockDesignDecision, response)
    }

    // Tests for processContext

    @Test
    fun `processContext should throw UnpassableMessageException if 4xx received from POST test`() {
        //Given
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(Context::class.java))).then { throw HttpClientErrorException(HttpStatus.BAD_REQUEST) }
        //When, Then
        assertThrows<UnparsableMessageException> {
            sut.processContext(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `processContext should throw Exception if 5xx recieved from POST test`() {
        //Given
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(Context::class.java))).then { throw HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR) }
        //When, Then
        assertThrows<Exception> {
            sut.processContext(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `processContext should throw Exception if null response body from POST test`() {
        //Given
        val mockResponse = mock<ResponseEntity<Context>>()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.OK)
        `when`(mockResponse.body).thenReturn(null)
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(Context::class.java))).thenReturn(mockResponse)
        //When, Then
        assertThrows<Exception> {
            sut.processContext(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `processContext should throw Exception if non OK status code from POST test`() {
        //Given
        val mockResponse = mock<ResponseEntity<Context>>()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED)
        `when`(mockResponse.body).thenReturn(mock())
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(Context::class.java))).thenReturn(mockResponse)
        //When, Then
        assertThrows<Exception> {
            sut.processContext(TEST_MESSAGE, UUID.randomUUID())
        }
    }

    @Test
    fun `processContext should return response body from POST if not null test`() {
        //Given
        val mockResponse: ResponseEntity<Context> = mock()
        val mockContext: Context = mock()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.OK)
        `when`(mockResponse.body).thenReturn(mockContext)
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(Context::class.java))).thenReturn(mockResponse)
        //When
        val response = sut.processContext(TEST_MESSAGE, UUID.randomUUID())
        //Then
        assertEquals(mockContext, response)
    }

    // Tests for processStringList

    @Test
    fun `processStringList should throw UnpassableMessageException if 4xx received from POST test`() {
        //Given
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(List::class.java))).then { throw HttpClientErrorException(HttpStatus.BAD_REQUEST) }
        //When, Then
        assertThrows<UnparsableMessageException> {
            sut.processStringList(TEST_MESSAGE, "option", use_and = true)
        }
    }

    @Test
    fun `processStringList should throw Exception if 5xx received from POST test`() {
        //Given
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(List::class.java))).then { throw HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR) }
        //When, Then
        assertThrows<Exception> {
            sut.processStringList(TEST_MESSAGE, "option", use_and = true)
        }
    }

    @Test
    fun `processStringList should throw Exception if null response body from POST test`() {
        //Given
        val mockResponse = mock<ResponseEntity<List<*>>>()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.OK)
        `when`(mockResponse.body).thenReturn(null)
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(List::class.java))).thenReturn(mockResponse)
        //When, Then
        assertThrows<Exception> {
            sut.processStringList(TEST_MESSAGE, "option", use_and = true)
        }
    }

    @Test
    fun `processStringList should throw Exception if non OK status code from POST test`() {
        //Given
        val mockResponse = mock<ResponseEntity<List<*>>>()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED)
        `when`(mockResponse.body).thenReturn(mock())
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(List::class.java))).thenReturn(mockResponse)
        //When, Then
        assertThrows<Exception> {
            sut.processStringList(TEST_MESSAGE, "option", use_and = true)
        }
    }

    @Test
    fun `processStringList should return response body from POST if not null test`() {
        //Given
        val mockResponse: ResponseEntity<List<*>> = mock()
        val mockList: List<*> = mock()
        `when`(mockResponse.statusCode).thenReturn(HttpStatus.OK)
        `when`(mockResponse.body).thenReturn(mockList)
        `when`(mockRestTemplate.postForEntity(anyString(), any(), eq(List::class.java))).thenReturn(mockResponse)
        //When
        val response = sut.processStringList(TEST_MESSAGE, "option", use_and = true)
        //Then
        assertEquals(mockList, response)
    }

}