package nz.ac.canterbury.csse.seng402.slackdecisionmaker.util

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals


class UtilTest {

    companion object {
        private val ENCRYPTION_KEY = "TESTTESTTESTTEST"
    }

    @Test
    fun parseCommaSeparatedTextTest() {
        // Given
        val inputText = "These, are, some, comma, separated, values"

        // When
        val result = parseCommaSeparatedText(inputText)

        // Then
        assertEquals(listOf("These", "are", "some", "comma", "separated", "values"), result)

    }

    @Test
    fun appendInputTextTest() {
        // Given
        val existingText = "Tammy"
        val newText = "Tester"

        // When
        val result = appendInputText(existingText, newText)

        // Then
        assertEquals("Tammy Tester", result)
    }

    @Test
    fun appendInputTextTestEmpty() {
        // Given
        val existingText = ""
        val newText = "Tammy"

        // When
        val result = appendInputText(existingText, newText)

        // Then
        assertEquals("Tammy", result)
    }

    @Test
    fun appendInputTextTestNull() {
        // Given
        val existingText = null
        val newText = "Tammy"

        // When
        val result = appendInputText(existingText, newText)

        // Then
        assertEquals("Tammy", result)
    }

    @Test
    fun appendInputTextTestNullString() {
        // Given
        val existingText = "null"
        val newText = "Tammy"

        // When
        val result = appendInputText(existingText, newText)

        // Then
        assertEquals("Tammy", result)
    }

    @Test
    fun testEncrypt() {
        // Given
        val unencryptedText = "Hello World"

        // When
        val result = encrypt(unencryptedText, ENCRYPTION_KEY)

        // Then
        assertNotEquals("Hello World", result)
    }

    @Test
    fun testDecrypt() {
        // Given
        val unencryptedText = "Hello World"

        // When
        val encryptedText = encrypt(unencryptedText, ENCRYPTION_KEY)
        val decryptedText = decrypt(encryptedText, ENCRYPTION_KEY)

        // Then
        assertNotEquals("Hello World", encryptedText)
        assertEquals(unencryptedText, decryptedText)
    }
}