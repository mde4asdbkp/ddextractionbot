package nz.ac.canterbury.csse.seng402.slackdecisionmaker

import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing


fun <T> any(): T {
    Mockito.any<T>()
    return uninitialized()
}

@Suppress("UNCHECKED_CAST")
private fun <T> uninitialized(): T = null as T

fun <T> OngoingStubbing<T>.doNothing(): OngoingStubbing<T> {
    return this.then { }
}

inline fun <reified T : Any> mock(): T {
    return Mockito.mock(T::class.java)
}