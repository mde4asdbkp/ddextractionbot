package nz.ac.canterbury.csse.seng402.slackdecisionmaker

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.AccountRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.DesignDecisionRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.DraftRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.ModelEntityManager
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.*
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.modeloutput.ModelOutputter
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage.NaturalLanguageProcessor
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage.UnparsableMessageException
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.EncryptionKey
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlashCommand
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.formatDesignDecisions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.*
import java.time.Instant
import java.util.*
import javax.lang.model.type.NullType

/**
 * Acceptance tests for the DomainModel logic
 *
 * Test functions intended to be self documenting
 */
class DomainModelTest {

    private val naturalLanguageProcessor: NaturalLanguageProcessor = mock()

    private val modelOutputter: ModelOutputter = mock()

    private val designDecisionRepository: DesignDecisionRepository = mock()

    private val modelEntityManager: ModelEntityManager = mock()

    private val accountRepository: AccountRepository = mock(AccountRepository::class.java, RETURNS_DEEP_STUBS)

    private val encryptionKey: EncryptionKey = EncryptionKey(key = "TEST")

    private val draftRepository: DraftRepository = mock()

    private val sut: DomainModel = DomainModel(naturalLanguageProcessor, modelOutputter, designDecisionRepository, modelEntityManager, accountRepository, encryptionKey, draftRepository)

    companion object {

        private val TEST_USER = User("1", "Jack Steel")

        private val TEST_ADD_DECISION_DATA = AddDecisionData(
                id = UUID.randomUUID(),
                text = "",
                metadata = Metadata(
                        recordingUser = TEST_USER,
                        involvedUsers = setOf(TEST_USER),
                        dateTime = Date.from(Instant.now()),
                        messageId = "1",
                        channelId = "1",
                        id = null
                )
        )

        private val TEST_DESIGN_DECISION = DesignDecision(
                id = 1,
                context = Context(id = 1, useCase = "UseCase", component = null),
                nonFunctionalConcernsAddressed = setOf(),
                chosenOption = Option(id = 1, name = "Name"),
                neglectedOptions = setOf(),
                qualityAttributesAchieved = setOf(),
                consequences = setOf(),
                inputText = "InputText",
                metadata = null
        )

        private val TEST_SEARCH_DECISION_DATA = SearchDecisionData(
                id = UUID.randomUUID(),
                query = "keyword1",
                metadata = Metadata(
                        recordingUser = TEST_USER,
                        involvedUsers = setOf(TEST_USER),
                        dateTime = Date.from(Instant.now()),
                        messageId = "1",
                        channelId = "1",
                        id = null
                )
        )

        private val TEST_ACCOUNT = Account(
                id = 1,
                channelId = "test",
                channelName = "test"
        )

        private val TEST_CREATE_ACCOUNT_DATA = CreateAccountData(
                id = UUID.randomUUID(),
                channelId = "test",
                channelName = "test"
        )

        private val TEST_DRAFT = Draft(
                id = 1,
                channelId = "test"
        )

        private val TEST_CREATE_DRAFT_DATA = CreateDraftData(
                id = UUID.randomUUID(),
                channelId = "test"
        )

        private val TEST_SLASH_COMMAND = SlashCommand(
                token = "token",
                text = "text",
                channel_id = "channel_id",
                channel_name = "channel_name",
                command = "command",
                response_url = "response_url",
                team_domain = "team_domain",
                team_id = "team_id",
                trigger_id = "trigger_id",
                user_id = "user_id",
                user_name = "username",
                enterprise_id = "enterprise_id",
                enterprise_name = "enterprise_name"
        )

        private val TEST_ADD_CONTEXT_DATA = AddContextData(
                id = UUID.randomUUID(),
                slashCommand = TEST_SLASH_COMMAND,
                text = "text"
        )

        private val TEST_ADD_NEGLECTED_OPTIONS_DATA = AddNeglectedOptionsData(
                id = UUID.randomUUID(),
                slashCommand = TEST_SLASH_COMMAND,
                text = "text"
        )

        private val TEST_ADD_QUALITY_ATTRIBUTES_DATA = AddQualityAttributesData(
                id = UUID.randomUUID(),
                slashCommand = TEST_SLASH_COMMAND,
                text = "text"
        )

        private val TEST_ADD_CONCERNS_DATA = AddConcernsData(
                id = UUID.randomUUID(),
                slashCommand = TEST_SLASH_COMMAND,
                text = "text"
        )

        private val TEST_ADD_CONSEQUENCES_DATA = AddConsequencesData(
                id = UUID.randomUUID(),
                slashCommand = TEST_SLASH_COMMAND,
                text = "text"
        )

        private val TEST_UPDATE_WIKI_CONFIGURATION_DATA = UpdateWikiConfigurationData(
                accountId = 1,
                channelId = "test",
                wikiURL = "test",
                wikiFilePath = "test",
                wikiUsername = "test",
                wikiPassword = "test"
        )

    }

    // Tests for recordDesign

    @Test
    fun `returns success false when nlp throws exception test`() {
        //Given
        `when`(naturalLanguageProcessor.processAddMessage(anyString(), any())).thenAnswer { throw UnparsableMessageException("") }
        //When
        val result = sut.recordDecision(TEST_ADD_DECISION_DATA)
        //Then
        assertFalse(result.wasSuccessful)
    }

    @Test
    fun `returns success false when an unexpected error is thrown`() {
        //Given
        `when`(naturalLanguageProcessor.processAddMessage(anyString(), any())).thenAnswer { throw Exception("Some error") }
        //When
        val result = sut.recordDecision(TEST_ADD_DECISION_DATA)
        //Then
        assertFalse(result.wasSuccessful)
    }

    @Test
    fun `returns success true when everything runs without error`() {
        //Given
        doReturn(TEST_DESIGN_DECISION).`when`(naturalLanguageProcessor).processAddMessage(anyString(), any())
        `when`(modelOutputter.outputModel(any())).doNothing()
        `when`(designDecisionRepository.findAll()).thenReturn(setOf(TEST_DESIGN_DECISION))
        `when`(modelEntityManager.persistSafely(any<DesignDecision>())).doNothing()
        //When
        val result = sut.recordDecision(TEST_ADD_DECISION_DATA)
        //Then
        assertTrue(result.wasSuccessful)
    }

    // Tests for searchDecision

    @Test
    fun `searchDecision returns success false when exception thrown during database search test`() {
        //Given
        `when`(designDecisionRepository.findDecisionsByQueryString(anyString(), anyString())).thenAnswer { throw Exception("Some Error") }
        //When
        val result = sut.searchDecision(TEST_SEARCH_DECISION_DATA)
        //Then
        assertFalse(result.wasSuccessful)
    }

    // TODO: Test for sucess when everything runs without error

    // Tests for createAccount

    @Test
    fun `createAccount returns success true when everything runs without error`() {
        //Given
        `when`(accountRepository.persistNewAccountIfNoneExistsWithChannelId(any())).thenReturn(TEST_ACCOUNT)
        //When
        val result = sut.createAccount(TEST_CREATE_ACCOUNT_DATA)
        //Then
        assertNotNull(result)
        assertTrue(result!!.wasSuccessful)
    }

    // Tests for createDraft

    @Test
    fun `createDraft returns success true when everything runs without error`() {
        //Given
        `when`(draftRepository.persistNewDraftIfNoneExistsWithChannelId(any())).thenReturn(TEST_DRAFT)
        //When
        val result = sut.createDraft(TEST_CREATE_DRAFT_DATA)
        //Then
        assertNotNull(result)
        assertTrue(result!!.wasSuccessful)
    }

    // TODO: Tests for addOption

    // Tests for addContext

    @Test
    fun `addContext returns null draft when nlp throws exception test`() {
        //Given
        `when`(naturalLanguageProcessor.processContext(anyString(), any())).thenAnswer { throw UnparsableMessageException("") }
        //When
        val result = sut.addContext(TEST_ADD_CONTEXT_DATA)
        //Then
        assertNull(result)
    }

    @Test
    fun `addContext returns null draft when unexpected exception thrown`() {
        //Given
        `when`(naturalLanguageProcessor.processContext(anyString(), any())).thenAnswer { throw Exception("Some exception") }
        //When
        val result = sut.addContext(TEST_ADD_CONTEXT_DATA)
        //Then
        assertNull(result)
    }

    // Tests for addNeglectedOptions

    @Test
    fun `addNeglectedOptions returns null draft when nlp throws exception test`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw UnparsableMessageException("") }
        //When
        val result = sut.addNeglectedOptions(TEST_ADD_NEGLECTED_OPTIONS_DATA)
        //Then
        assertNull(result)
    }

    @Test
    fun `addNeglectedOptions returns null draft when unexpected exception thrown`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw Exception("Some Exception") }
        //When
        val result = sut.addNeglectedOptions(TEST_ADD_NEGLECTED_OPTIONS_DATA)
        //Then
        assertNull(result)
    }

    // Tests for addQualityAttributes

    @Test
    fun `addQualityAttributes returns null draft when nlp throws exception test`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw UnparsableMessageException("") }
        //When
        val result = sut.addQualityAttributes(TEST_ADD_QUALITY_ATTRIBUTES_DATA)
        //Then
        assertNull(result)
    }

    @Test
    fun `addQualityAttributes returns null draft when unexpected exception thrown`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw Exception("Some exception") }
        //When
        val result = sut.addQualityAttributes(TEST_ADD_QUALITY_ATTRIBUTES_DATA)
        //Then
        assertNull(result)
    }

    // Tests for addConcerns

    @Test
    fun `addConcerns returns null draft when nlp throws exception test`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw UnparsableMessageException("") }
        //When
        val result = sut.addConcerns(TEST_ADD_CONCERNS_DATA)
        //Then
        assertNull(result)
    }

    @Test
    fun `addConcerns returns null draft when unexpected exception thrown`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw Exception("Some exception") }
        //When
        val result = sut.addConcerns(TEST_ADD_CONCERNS_DATA)
        //Then
        assertNull(result)
    }

    // Tests for addConsequences

    @Test
    fun `addConsequences returns null draft when nlp throws exception test`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw UnparsableMessageException("") }
        //When
        val result = sut.addConsequences(TEST_ADD_CONSEQUENCES_DATA)
        //Then
        assertNull(result)
    }

    @Test
    fun `addConsequences returns null draft when unexpected exception thrown`() {
        //Given
        `when`(naturalLanguageProcessor.processStringList(anyString(), any(), anyBoolean())).thenAnswer { throw Exception("Some exception") }
        //When
        val result = sut.addConsequences(TEST_ADD_CONSEQUENCES_DATA)
        //Then
        assertNull(result)
    }

    // TODO: Tests for updateWikiConfiguration

    // Tests for abort

    @Test
    fun `abort returns success false when an unexpected error is thrown`() {
        //Given
        `when`(draftRepository.findByChannelId(anyString())).thenAnswer { throw Exception("Some error") }
        //When
        val result = sut.abort(TEST_SLASH_COMMAND)
        //Then
        assertFalse(result.wasSuccessful)
    }

// TODO: Make this test work
//    @Test
//    fun `abort returns success true when everything runs without error`() {
//        //Given
//        `when`(draftRepository.findByChannelId(anyString())).thenReturn(TEST_DRAFT)
//        `when`(draftRepository.save(any<Draft>())).doNothing()
//        //When
//        val result = sut.abort(TEST_SLASH_COMMAND)
//        //Then
//        assertTrue(result.wasSuccessful)
//    }

    // Tests for confirm

    @Test
    fun `confirm returns success false when an unexpected error is thrown`() {
        //Given
        `when`(draftRepository.findByChannelId(anyString())).thenAnswer { throw Exception("Some error") }
        //When
        val result = sut.confirm(TEST_SLASH_COMMAND)
        //Then
        assertFalse(result.wasSuccessful)
    }




}
