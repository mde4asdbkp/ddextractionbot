package nz.ac.canterbury.csse.seng402.slackdecisionmaker.modeloutput

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision

/**
 * Provides static parsing of DesignDecisions to Markdown as a [String]
 */
object MarkdownParser {

    fun parseToMarkdown(designDecisions: List<DesignDecision>): String {
        val head = "# Design decisions\n\n"
        return head + designDecisions.joinToString(separator = "\n\n") {
            parseToMarkdown(it)
        }
    }

    private fun parseToMarkdown(designDecision: DesignDecision): String {
        var markdown = ""

        markdown += "## Use case \n${designDecision.context.useCase}\n\n"

        markdown += "### Chosen option\n${designDecision.chosenOption.name}\n\n"

        markdown += "### Neglected options\n"
        markdown += designDecision.neglectedOptions.joinToString(separator = "\n") {
            "* ${it.name}"
        } + "\n\n"

        markdown += "### Non functional concerns addressed\n"
        markdown += designDecision.nonFunctionalConcernsAddressed.joinToString(separator = "\n") {
            "* ${it.concern}"
        } + "\n\n"

        markdown += "### Quality attributes achieved\n"
        markdown += designDecision.qualityAttributesAchieved.joinToString(separator = "\n") {
            "* ${it.attribute}"
        } + "\n\n"

        markdown += "### Consequences\n"
        markdown += designDecision.consequences.joinToString(separator = "\n") {
            "* ${it.consequence}"
        } + "\n\n"

        markdown += "#### Initial text\n`${designDecision.inputText}`"

        return markdown
    }

}