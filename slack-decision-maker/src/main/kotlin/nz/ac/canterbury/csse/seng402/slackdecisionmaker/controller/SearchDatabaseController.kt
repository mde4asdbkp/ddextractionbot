package nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.DomainModel
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.SearchDecisionData
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Metadata
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.User
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlackResponse
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlashCommand
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.time.Instant
import java.util.*
import kotlin.concurrent.thread


/**
 * The Slack endpoint to search the design decision database
 *
 * Takes tge [DomainModel] to trigger actions and a [RestTemplate] to POST back to Slack
 */
@RestController
class SearchDatabaseController(
        private val domainModel: DomainModel,
        private val restTemplate: RestTemplate
) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SearchDatabaseController::class.java)
        const val SEARCH_ENDPOINT = "/search"
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.searchDecision] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simply OK processing message to notify the Slack yser that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(SEARCH_ENDPOINT)
    fun searchDecisionDatabase(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()
        logger.debug("$id: Searching design database: $slashCommand")
        val searchingUser = User(slashCommand.user_name, slashCommand.user_id)
        thread(start = true) {
            logger.info(slashCommand.text)
            val result = domainModel.searchDecision(SearchDecisionData(
                    id,
                    slashCommand.text,
                    // Including meta data for expansion of further authentication in future
                    Metadata(
                            searchingUser,
                            setOf(searchingUser), //TODO: Include other users
                            Date.from(Instant.now()),
                            slashCommand.token,
                            slashCommand.channel_id
                    )
            ))
            val response = SlackResponse(
                    text = result.responseText,
                    response_type = if (result.wasSuccessful) "in_channel" else "ephemeral"
            )
            logger.debug("$id: Responding with result: $result, with response: $response")
            restTemplate.postForLocation(slashCommand.response_url, response)
        }

        RecordDesignController.logger.debug("$id: Replying with initial processing message for ${slashCommand.response_url}")
        return ResponseEntity.ok("Processing!")
    }

}