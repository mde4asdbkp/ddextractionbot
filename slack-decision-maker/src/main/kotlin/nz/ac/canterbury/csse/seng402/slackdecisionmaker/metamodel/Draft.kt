package nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel

import javax.persistence.*

/**
 * The data model for a Draft design decision, used in the chat-based recording system.
 *
 * Contains [javax.persistence] annotations to allow Spring JPA to persist data.
 */
@Entity
data class Draft (

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val channelId: String,

        var contextUseCase: String? = null,

        var contextComponent: String? = null,

        var option: String? = null,

        @ElementCollection(fetch = FetchType.EAGER)
        var neglectedOptions: Set<String>? = null,

        @ElementCollection(fetch = FetchType.EAGER)
        var qualityAttributes: Set<String>? = null,

        @ElementCollection(fetch = FetchType.EAGER)
        var concerns: Set<String>? = null,

        @ElementCollection(fetch = FetchType.EAGER)
        var consequences: Set<String>? = null,

        var inputText: String? = null
)