package nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel

import javax.persistence.*

/**
 * The data model for an overall DesignDecision.
 *
 * Contains [javax.persistence] annotations to allow Spring JPA to persist data.
 */
@Entity
data class DesignDecision(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        @JoinColumn
        val context: Context,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, orphanRemoval = true)
        @JoinColumn
        val nonFunctionalConcernsAddressed: Set<NonFunctionalConcern>,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn
        val chosenOption: Option,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, orphanRemoval = true)
        @JoinColumn(name = "design_option")
        val neglectedOptions: Set<Option>,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, orphanRemoval = true)
        @JoinColumn
        val qualityAttributesAchieved: Set<QualityAttribute>,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, orphanRemoval = true)
        @JoinColumn
        val consequences: Set<Consequence>,

        @Column(columnDefinition="LONGTEXT")
        val inputText: String,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn
        var metadata: Metadata?
)

@Entity
data class Context(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val useCase: String,
        val component: String?
)

@Entity
data class NonFunctionalConcern(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val concern: String
)

@Entity(name = "design_option")
@Table(name = "design_option")
data class Option(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val name: String
)

@Entity
data class QualityAttribute(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val attribute: String
)

@Entity
data class Consequence(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val consequence: String
)