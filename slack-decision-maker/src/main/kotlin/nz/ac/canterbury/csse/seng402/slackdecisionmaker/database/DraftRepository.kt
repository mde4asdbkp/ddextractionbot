package nz.ac.canterbury.csse.seng402.slackdecisionmaker.database

import io.netty.channel.ChannelId
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller.SearchDatabaseController
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Draft
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.NoResultException
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root
import javax.transaction.Transactional

/**
 * Custom Draft repository interface allowing extensions of simple [CrudRepository]
 */
interface DraftRepositoryExtended {
    fun persistNewDraftIfNoneExistsWithChannelId(@Param("draft") draft: Draft): Draft

    fun findByChannelId(@Param("channelId") channelId: String): Draft?
}

/**
 * Simple Spring [CrudRepository] for Drafts. Auto generates everything based on [javax.persistence] annotations
 */
@Repository
interface DraftRepository: CrudRepository<Draft, Long>, DraftRepositoryExtended

/**
 * Implementation of custom Draft Repository methods
 */
open class DraftRepositoryImpl(private val entityManager: EntityManager): DraftRepositoryExtended {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SearchDatabaseController::class.java)
    }

    /**
     * Given a draft model, create a new entry in the draft table for the channel id associated with the draft
     * if one does not already exist.
     *
     * Either returns the existing draft, or the newly created draft.
     */
    @Transactional
    override fun persistNewDraftIfNoneExistsWithChannelId(draft: Draft): Draft {

        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val query: CriteriaQuery<Draft> = criteriaBuilder.createQuery(Draft::class.java)
        val draftRoot: Root<Draft> = query.from(Draft::class.java)

        query.select(draftRoot).where(
                criteriaBuilder.equal(draftRoot.get<String>("channelId"), draft.channelId)
        )

        return try {
            entityManager.createQuery(query).singleResult
        } catch (e: NoResultException) {
            entityManager.merge(draft)
        }
    }

    /**
     * Finds and returns a Draft by the given channelId. Returns null if no such Draft exists.
     */
    override fun findByChannelId(channelId: String): Draft? {
        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val query: CriteriaQuery<Draft> = criteriaBuilder.createQuery(Draft::class.java)
        val draftRoot: Root<Draft> = query.from(Draft::class.java)

        query.select(draftRoot).where(
                criteriaBuilder.equal(draftRoot.get<String>("channelId"), channelId)
        )

        return try {
            entityManager.createQuery(query).singleResult
        } catch (e: NoResultException) {
            null
        }
    }

}