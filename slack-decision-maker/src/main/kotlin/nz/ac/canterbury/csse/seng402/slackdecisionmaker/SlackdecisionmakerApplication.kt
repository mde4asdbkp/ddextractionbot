package nz.ac.canterbury.csse.seng402.slackdecisionmaker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SlackdecisionmakerApplication

/**
 * Application entry point
 */
fun main(args: Array<String>) {
	runApplication<SlackdecisionmakerApplication>(*args)
}
