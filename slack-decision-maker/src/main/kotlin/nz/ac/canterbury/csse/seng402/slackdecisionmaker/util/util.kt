package nz.ac.canterbury.csse.seng402.slackdecisionmaker.util

import io.netty.handler.codec.base64.Base64Encoder
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller.SearchDatabaseController
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.*
import org.bouncycastle.util.Arrays
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.charset.Charset
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.DESKeySpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import kotlin.text.Charsets.UTF_8


val logger: Logger = LoggerFactory.getLogger(SearchDatabaseController::class.java)


/**
 * Helper function to format a list of design decisions into text better suited for outputting to slack
 */
fun formatDesignDecisions(decisions: List<DesignDecision>): String {

    var outputText: String = ""
    logger.info(decisions.toString())
    for (decision in decisions) {
        outputText = outputText.plus(
                "*Context (Component):* ${decision.context.component}\n" +
                        "*Context (Use Case):* ${decision.context.useCase}\n" +
                        "*Chosen Option:* ${decision.chosenOption.name}\n" +
                        "*Recording User*: ${decision.metadata!!.recordingUser.id}\n" +
                        "*Timestamp*: ${decision.metadata!!.dateTime}\n"
        )
        outputText = outputText.plus("\n")
    }

    return if (outputText.isNullOrEmpty()) "No decisions matched search string!" else outputText

}

/**
 * Helper function for parsing comma separated string values.
 * Used to parse wiki configuration provided in setup method.
 */
fun parseCommaSeparatedText(text: String): List<String> {
    return text.replace("\\s".toRegex(), "").split(',')
}

/**
 * Helper function to encrypt a given string using a given key
 */
fun encrypt(text: String, key: String): String {
    val cipher: Cipher = Cipher.getInstance("AES")
    val aesKey = SecretKeySpec(Arrays.copyOf(key.toByteArray(), 16), "AES")

    cipher.init(Cipher.ENCRYPT_MODE, aesKey)
    val encrypted = cipher.doFinal(text.toByteArray())
    val encoded = Base64.getEncoder().encode(encrypted)
    return String(encoded)
}

/**
 * Helper function to decrypt a string that had been encrypted using the key provided
 */
fun decrypt(text: String, key: String): String {
    val cipher: Cipher = Cipher.getInstance("AES")
    val aesKey = SecretKeySpec(Arrays.copyOf(key.toByteArray(), 16), "AES")

    val decodedValue = Base64.getDecoder().decode(text.toByteArray())
    cipher.init(Cipher.DECRYPT_MODE, aesKey)
    val decrypted = cipher.doFinal(decodedValue)
    return String(decrypted)
}

/**
 * Take a draft and create text to be output by a command to provide further instructions to a user
 */
fun draftCommandOutputText(draft: Draft): String {

    var outputText: String = ""

    if (draft.contextComponent.isNullOrEmpty() && draft.contextUseCase.isNullOrEmpty() ) {

        outputText += "\tContext is missing from the decision, this is required.\n"
        outputText += "\tUse `'/context'` to provide either the context use case, component, or both. (required)\n\n"

    }

    if (draft.option.isNullOrEmpty()) {

        outputText += "\tThe chosen option is missing from the decision, this is required.\n"
        outputText += "\tUse `'/option'` to provide the chosen option (required)\n\n"

    }

    if (draft.concerns == null || draft.concerns!!.isEmpty()) {

        outputText += "\tNo concerns have been added to the decision\n"
        outputText += "\tUse `'/concern'` to provide concerns (optional)\n\n"

    }

    if (draft.qualityAttributes == null || draft.qualityAttributes!!.isEmpty()) {

        outputText += "\tNo quality attributes have been added to the decision\n"
        outputText += "\tUse `'/quality'` to provide quality attributes (optional)\n\n"

    }

    if (draft.neglectedOptions == null || draft.neglectedOptions!!.isEmpty()) {

        outputText += "\tNo neglected options have been added to the decision\n"
        outputText += "\tUse `'/neglected'` to provide neglected options (optional)\n\n"

    }

    if (draft.consequences == null || draft.consequences!!.isEmpty()) {

        outputText += "\tNo consequences have been added to the decision\n"
        outputText += "\tUse `'/consequence'` to provide consequences (optional)\n\n"

    }

    outputText += "\tUse `'/confirm'` to finish and persist the decision\n"
    outputText += "\tUse `'/abort'` to discard the current decision dialogue\n"

    return outputText
}

/**
 * Helper function to convert a given, completed draft, into a design decision
 */
fun draftToDecision(draft: Draft): DesignDecision {

    val id = Random().nextLong()

    val context = Context(
            id = Random().nextLong(),
            useCase = draft.contextUseCase!!,
            component = draft.contextComponent
    )

    val chosenOption = Option(
            id = Random().nextLong(),
            name = draft.option!!
    )

    val nonFunctionalConcerns: Set<NonFunctionalConcern> = HashSet(ArrayList(draft.concerns!!).map { stringConcern ->
        NonFunctionalConcern(
                id = Random().nextLong(),
                concern = stringConcern
        )
    })

    val neglectedOptions: Set<Option> = HashSet(ArrayList(draft.neglectedOptions!!).map { stringOption ->
        Option(
                id = Random().nextLong(),
                name = stringOption
        )
    })

    val qualityAttributes: Set<QualityAttribute> = HashSet(ArrayList(draft.qualityAttributes!!).map { stringQuality ->
        QualityAttribute(
                id = Random().nextLong(),
                attribute = stringQuality
        )
    })

    val consequences: Set<Consequence> = HashSet(ArrayList(draft.consequences!!).map { stringConsequence ->
        Consequence(
                id = Random().nextLong(),
                consequence = stringConsequence
        )
    })

    var designDecision = DesignDecision(
            id = id,
            context = context,
            chosenOption = chosenOption,
            nonFunctionalConcernsAddressed = nonFunctionalConcerns,
            neglectedOptions = neglectedOptions,
            qualityAttributesAchieved = qualityAttributes,
            consequences = consequences,
            inputText = draft.inputText!!,
            metadata = null
    )

    return designDecision
}

/**
 * Helper function for appending text from each command to existing text
 */
fun appendInputText(existingText: String?, newText: String): String {
    if (existingText.isNullOrEmpty() || existingText == "null") {
        return newText
    } else {
        return "$existingText $newText"
    }
}


/**
 * Conforms to the Slack slash command message response payload format
 */
data class SlackResponse(
        val text: String,
        val response_type: String = "in_channel"
)

/**
 * Conforms to the Slack slash command data payload
 *
 * @see <a href="https://api.slack.com/slash-commands#app_command_handling">Slack slash command API data payload</a>
 */
data class SlashCommand(
        val token: String,
        val team_id: String,
        val team_domain: String,
        val enterprise_id: String?,
        val enterprise_name: String?,
        val channel_id: String,
        val channel_name: String,
        val user_id: String,
        val user_name: String,
        val command: String,
        val text: String,
        val response_url: String,
        val trigger_id: String
)

data class WikiConfiguration(
        val url: String,
        val username: String,
        val password: String,
        val relativeFilePath: String
)

data class EncryptionKey(
        val key: String
)