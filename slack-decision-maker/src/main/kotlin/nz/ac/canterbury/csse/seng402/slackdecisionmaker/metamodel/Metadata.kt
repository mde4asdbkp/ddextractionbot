package nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel

import java.util.*
import javax.persistence.*

/**
 * Metadata for any message.
 */
@Entity
data class Metadata(
        @OneToOne(cascade = [CascadeType.ALL])
        val recordingUser: User,

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        val involvedUsers: Set<User>,

        val dateTime: Date,

        val messageId: String,

        val channelId: String,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null
)

@Entity
@Table(name = "slack_user")
data class User(
        @Id
        val id: String,
        val name: String
)