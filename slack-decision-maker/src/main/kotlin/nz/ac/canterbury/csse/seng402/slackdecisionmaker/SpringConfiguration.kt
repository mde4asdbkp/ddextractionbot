package nz.ac.canterbury.csse.seng402.slackdecisionmaker

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.EncryptionKey
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.WikiConfiguration
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.web.client.RestTemplate
import javax.sql.DataSource


@Configuration
class SpringConfiguration {

    @Bean
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate {
        return builder.build()
    }

    @Bean
    @ConditionalOnMissingBean
    fun encryptionKey(
            @Value("\${ENCRYPTION_KEY}") key: String
    ): EncryptionKey {
        return EncryptionKey(
                key = key
        )
    }

    /**
     * TODO: Remove this code as it is no longer need with multi-user functionality
     */
    @Bean
    @ConditionalOnMissingBean
    fun wikiConfiguration(
            @Value("\${WIKI_URL}") wikiUrl: String,
            @Value("\${WIKI_FILE_PATH}") wikiRelativeFilePath: String,
            @Value("\${WIKI_USERNAME}") wikiUsername: String,
            @Value("\${WIKI_PASSWORD}") wikiPassword: String
    ): WikiConfiguration {
        return WikiConfiguration(
                url = wikiUrl,
                username = wikiUsername,
                password = wikiPassword,
                relativeFilePath = wikiRelativeFilePath
        )
    }

    @Bean
    @ConditionalOnMissingBean
    fun dataSource(
            @Value("\${DB_USERNAME}") dbUsername: String,
            @Value("\${DB_PASSWORD}") dbPassword: String,
            @Value("\${DB_URL}") dbUrl: String
    ): DataSource {
        return DriverManagerDataSource().apply {
            setDriverClassName("org.mariadb.jdbc.Driver")
            username = dbUsername
            password = dbPassword
            url = dbUrl
        }
    }

    @Bean
    @Primary
    fun objectMapper(): ObjectMapper {
        return ObjectMapper().registerModule(KotlinModule())
    }
}