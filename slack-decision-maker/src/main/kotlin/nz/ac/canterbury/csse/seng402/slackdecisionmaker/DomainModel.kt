package nz.ac.canterbury.csse.seng402.slackdecisionmaker

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.AccountRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.DesignDecisionRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.DraftRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.ModelEntityManager
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.*
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.modeloutput.ModelOutputter
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage.NaturalLanguageProcessor
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage.UnparsableMessageException
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*
import kotlin.random.Random

/**
 * The main brain for the interaction between systems.
 *
 * Handles the workflow once a request is made from a Slack command.
 */
@Service
class DomainModel(
        private val languageProcessor: NaturalLanguageProcessor,
        private val modelOutputter: ModelOutputter,
        private val designDecisionRepository: DesignDecisionRepository,
        private val modelEntityManager: ModelEntityManager,
        private val accountRepository: AccountRepository,
        private val encryptionKey: EncryptionKey,
        private val draftRepository: DraftRepository
) {

    val logger: Logger = LoggerFactory.getLogger(DomainModel::class.java)

    /**
     * Accept the [AddDecisionData] and manage the workflow to process language,
     * save the decision and output it, and then return an [AddDecisionResult]
     */
    fun recordDecision(addDecisionData: AddDecisionData): AddDecisionResult {
        val id = addDecisionData.id
        try {
            logger.info("$id: Recording decision: $addDecisionData")
            val processedMessageWithoutMetadata = languageProcessor.processAddMessage(addDecisionData.text, addDecisionData.id)
            val processedMessage = processedMessageWithoutMetadata.copy(metadata = addDecisionData.metadata)
            logger.debug("$id: Processed message to a design decision, saving to DB: $processedMessage")
            modelEntityManager.persistSafely(processedMessage)
            logger.debug("$id: Saved to DB, pushing to model output")
            //modelOutputter.outputModel(designDecisionRepository.findAll().toList())
            modelOutputter.outputModel(designDecisionRepository.findDecisionsByChannelId(addDecisionData.metadata.channelId))
            logger.debug("$id: Pushed to model output, returning now")
            logger.info("$id: Successfully handled message, returning result to caller")
            return AddDecisionResult(
                    addDecisionData.id,
                    wasSuccessful = true,
                    responseText = "Successfully recorded decision ${processedMessage.chosenOption.name} for ${processedMessage.context.useCase}."
            )
        } catch (e: UnparsableMessageException) {
            logger.debug("$id: Message was unparsable: $e")
            return AddDecisionResult(addDecisionData.id, wasSuccessful = false, responseText = e.message
                    ?: "Message wasn't parsable!")
        } catch (e: Exception) {
            logger.error("$id: Had exception while processing: $addDecisionData", e)
            return AddDecisionResult(
                    id = addDecisionData.id,
                    wasSuccessful = false,
                    responseText = e.message ?: "Something went wrong! Please notify us if this issue persists."
            )
        }
    }

    /**
     * Accept the [SearchDecisionData] and manage the workflow to search the decision database,
     * query the decision database and output the results in a [SearchDecisionResult]
     */
    fun searchDecision(searchDecisionData: SearchDecisionData): SearchDecisionResult {
        val id = searchDecisionData.id
        try {
            logger.info("$id: Searching decisions: $searchDecisionData")
            var decisions: List<DesignDecision>
            val queryString = searchDecisionData.query
            if (queryString.isEmpty()) {
                decisions = designDecisionRepository.findDecisionsByChannelId(searchDecisionData.metadata.channelId)
            } else {
                decisions = designDecisionRepository.findDecisionsByQueryString(queryString, searchDecisionData.metadata.channelId)
            }
            logger.debug("$id: Queried database successfully")
            val responseText = formatDesignDecisions(decisions)
            logger.debug("$id: Formatted decisions for output to Slack channel")
            return SearchDecisionResult(
                    id = id,
                    wasSuccessful = true,
                    responseText = responseText
            )
        } catch (e: Exception) {
            logger.error("Exception when attempting to search decision database")
            return SearchDecisionResult(
                    id = id,
                    wasSuccessful = false,
                    responseText = e.message ?: "Something went wrong! Please notify us if this issue persists."
            )
        }
    }

    /**
     * Accept the [CreateAccountData] and manage the workflow to create an Account,
     * and output the result in a [CreateAccountResult]
     */
    fun createAccount(createAccountData: CreateAccountData): CreateAccountResult? {
        val account: Account = Account(
                id = Random.nextLong(),
                channelId = createAccountData.channelId,
                channelName = createAccountData.channelName

        )
        // Change to persist only if account with channelId does not already exist
        val savedAccount = accountRepository.persistNewAccountIfNoneExistsWithChannelId(account)
        return CreateAccountResult(
                id = savedAccount.id!!,
                wasSuccessful = true,
                responseText = "Successfully saved account"
        )

    }

    /**
     * Accept the [CreateDraftData] and manage the workflow to create a Draft,
     * and output the result in a [CreateDraftResult]
     */
    fun createDraft(createDraftData: CreateDraftData): CreateDraftResult? {
        val draft: Draft = Draft(
                id = Random.nextLong(),
                channelId = createDraftData.channelId,
                inputText = ""
        )

        val savedDraft: Draft = draftRepository.persistNewDraftIfNoneExistsWithChannelId(draft)

        return CreateDraftResult(
                id = savedDraft.id,
                createdDraft = savedDraft,
                wasSuccessful = true,
                responseText = "Returning draft entity"
        )
    }

    /**
     * Accept the [SlashCommand] and manage the workflow to add an option to
     * the draft associated with the channel id found in the command. Outputs
     * the now potentially updated state of [Draft]
     */
    fun addOption(slashCommand: SlashCommand): Draft {

        var draftEntity = draftRepository.findByChannelId(slashCommand.channel_id)
        draftEntity?.let {
            it.option = slashCommand.text
            it.inputText = appendInputText(it.inputText, slashCommand.text)
        }

        draftRepository.save(draftEntity!!)

        return draftEntity
    }

    /**
     * Accept the [AddContextData] and manage the workflow to add context to
     * the draft associated with the channel id given. Output the now potentially
     * updated state of [Draft]
     */
    fun addContext(addContextData: AddContextData): Draft? {
        val id = addContextData.id
        try {
            val processedContext: Context = languageProcessor.processContext(addContextData.text, id)
            var draftEntity = draftRepository.findByChannelId(addContextData.slashCommand.channel_id)

            draftEntity?.let{ draft ->
                draft.contextUseCase = processedContext.useCase
                draft.inputText = appendInputText(draft.inputText, addContextData.slashCommand.text)
                processedContext.component?.let {
                    draft.contextComponent = it
                }

            }

            draftRepository.save(draftEntity!!)
            return draftEntity


        } catch (e: UnparsableMessageException) {
            logger.debug("$id: Message was unparsable: $e")
            return null
        } catch (e: Exception) {
            logger.error("$id: Had exception while processing: $addContextData", e)
            return null
        }

    }

    /**
     * Accept the [AddNeglectedOptionsData] and manage the workflow to add neglected options to
     * the draft associated with the channel id given. Output the now potentially
     * updated state of [Draft]
     */
    fun addNeglectedOptions(addNeglectedOptionsData: AddNeglectedOptionsData): Draft? {
        val id = addNeglectedOptionsData.id
        try {
            val processedNeglectedOptions: List<String> = languageProcessor.processStringList(
                    addNeglectedOptionsData.text,
                    type = "neglected",
                    use_and = true
            )
            var draftEntity = draftRepository.findByChannelId(addNeglectedOptionsData.slashCommand.channel_id)

            draftEntity?.let{ draft ->
                draft.inputText = appendInputText(draft.inputText, addNeglectedOptionsData.slashCommand.text)
                draft.neglectedOptions = HashSet<String>(processedNeglectedOptions)
            }

            draftRepository.save(draftEntity!!)
            return draftEntity

        } catch (e: UnparsableMessageException) {
            logger.debug("$id: Message was unparsable: $e")
            return null
        } catch (e: Exception) {
            logger.error("$id: Had exception while processing: $addNeglectedOptionsData", e)
            return null
        }
    }

    /**
     * Accept the [AddQualityAttributesData] and manage the workflow to add quality attributes to
     * the draft associated with the channel id given. Output the now potentially
     * updated state of [Draft]
     */
    fun addQualityAttributes(addQualityAttributesData: AddQualityAttributesData): Draft? {
        val id = addQualityAttributesData.id
        try {
            val processedQualityAttributes: List<String> = languageProcessor.processStringList(
                    addQualityAttributesData.text,
                    type = "quality",
                    use_and = true
            )
            var draftEntity = draftRepository.findByChannelId(addQualityAttributesData.slashCommand.channel_id)

            draftEntity?.let{ draft ->
                draft.inputText = appendInputText(draft.inputText, addQualityAttributesData.slashCommand.text)
                draft.qualityAttributes = HashSet<String>(processedQualityAttributes)
            }

            draftRepository.save(draftEntity!!)
            return draftEntity

        } catch (e: UnparsableMessageException) {
            logger.debug("$id: Message was unparsable: $e")
            return null
        } catch (e: Exception) {
            logger.error("$id: Had exception while processing: $addQualityAttributesData", e)
            return null
        }
    }

    /**
     * Accept the [AddConcernsData] and manage the workflow to add concerns to
     * the draft associated with the channel id given. Output the now potentially
     * updated state of [Draft]
     */
    fun addConcerns(addConcernsData: AddConcernsData): Draft? {
        val id = addConcernsData.id
        try {
            val processedConcerns: List<String> = languageProcessor.processStringList(
                    addConcernsData.text,
                    type = "concern",
                    use_and = false
            )
            var draftEntity = draftRepository.findByChannelId(addConcernsData.slashCommand.channel_id)

            draftEntity?.let{ draft ->
                draft.inputText = appendInputText(draft.inputText, addConcernsData.slashCommand.text)
                draft.concerns = HashSet<String>(processedConcerns)
            }

            draftRepository.save(draftEntity!!)
            return draftEntity

        } catch (e: UnparsableMessageException) {
            logger.debug("$id: Message was unparsable: $e")
            return null
        } catch (e: Exception) {
            logger.error("$id: Had exception while processing: $addConcernsData", e)
            return null
        }
    }

    /**
     * Accept the [AddConsequencesData] and manage the workflow to add consequences to
     * the draft associated with the channel id given. Output the now potentially
     * updated state of [Draft]
     */
    fun addConsequences(addConsequencesData: AddConsequencesData): Draft? {
        val id = addConsequencesData.id
        try {
            val processedConcerns: List<String> = languageProcessor.processStringList(
                    addConsequencesData.text,
                    type = "consequence",
                    use_and = false
            )
            var draftEntity = draftRepository.findByChannelId(addConsequencesData.slashCommand.channel_id)

            draftEntity?.let{ draft ->
                draft.inputText = appendInputText(draft.inputText, addConsequencesData.slashCommand.text)
                draft.consequences = HashSet<String>(processedConcerns)
            }

            draftRepository.save(draftEntity!!)
            return draftEntity

        } catch (e: UnparsableMessageException) {
            logger.debug("$id: Message was unparsable: $e")
            return null
        } catch (e: Exception) {
            logger.error("$id: Had exception while processing: $addConsequencesData", e)
            return null
        }
    }



    /**
     * Accept the [UpdateWikiConfigurationData] and manage the workflow to add a new wiki configuration
     * into the database for the given account. Outputs the result of this in the [UpdateWikiConfigurationResult]
     */
    fun updateWikiConfiguration(updateWikiConfigurationData: UpdateWikiConfigurationData): UpdateWikiConfigurationResult {

        val accountEntity = accountRepository.findById(updateWikiConfigurationData.accountId).get()

        // Encrypt the password

        val encryptedPassword = encrypt(updateWikiConfigurationData.wikiPassword, encryptionKey.key)

        // Update the fields

        accountEntity.wikiURL = updateWikiConfigurationData.wikiURL
        accountEntity.wikiFilePath = updateWikiConfigurationData.wikiFilePath
        accountEntity.wikiUsername = updateWikiConfigurationData.wikiUsername
        accountEntity.wikiPassword = encryptedPassword

        // Save it back to the database
        accountRepository.save(accountEntity)

        return UpdateWikiConfigurationResult(
                wasSuccessful = true,
                responseText = "temp"
        )
    }

    /**
     * Take the [SlashCommand] as input and manage the workflow to abort the current draft associated
     * with the given account. Outputs the result with an [AbortResult]
     */
    fun abort(slashCommand: SlashCommand): AbortResult {

        try {

            var draftEntity = draftRepository.findByChannelId(slashCommand.channel_id)!!

            draftEntity.option = null
            draftEntity.contextUseCase = null
            draftEntity.contextComponent = null
            draftEntity.concerns = null
            draftEntity.consequences = null
            draftEntity.qualityAttributes = null
            draftEntity.neglectedOptions = null
            draftEntity.inputText = null

            draftRepository.save(draftEntity)

            return AbortResult(
                    responseText = "Successfully aborted design decision draft",
                    wasSuccessful = true
            )
        } catch (e: Exception) {
            return AbortResult(
                    responseText = "Something went wrong",
                    wasSuccessful = false
            )
        }
    }

    /**
     * Take the [SlashCommand] as input and manage the workflow to confirm and persist the current draft associated
     * with the given account. Outputs the result with an [ConfirmResult]
     */
    fun confirm(slashCommand: SlashCommand): ConfirmResult {
        try {

            // Retrieve draft from db
            var draftEntity = draftRepository.findByChannelId(slashCommand.channel_id)!!

            // Ensure required aspects are present, exit if not with explanation
            if (draftEntity.option.isNullOrEmpty() || (draftEntity.contextComponent.isNullOrEmpty() && draftEntity.contextUseCase.isNullOrEmpty())) {
                return ConfirmResult(
                        responseText = "Required aspects of draft are missing! Ensure you have provided at least context and the chosen option",
                        wasSuccessful = true // Intentionally true to ensure response in channel - change this implementation in future
                )
            }

            // Call util function to map draft to design decision
            var mappedDecision: DesignDecision = draftToDecision(draftEntity)

            // Add metadata
            val recordingUser = User(slashCommand.user_name, slashCommand.user_id)
            val metadata = Metadata(
                    recordingUser,
                    setOf(recordingUser), //TODO: Include other users
                    Date.from(Instant.now()),
                    slashCommand.token,
                    slashCommand.channel_id
            )
            mappedDecision.metadata = metadata

            // Persist decision - split into helper function in future as same code in record decision function

            modelEntityManager.persistSafely(mappedDecision)
            modelOutputter.outputModel(designDecisionRepository.findDecisionsByChannelId(slashCommand.channel_id))

            // Clear draft (same helper method to be used in abort)
            abort(slashCommand)
            return ConfirmResult(
                    responseText = "Successfully persisted design decision!",
                    wasSuccessful = true
            )
        } catch (e: Exception) {
            return ConfirmResult(
                    responseText = "Something went wrong",
                    wasSuccessful = false
            )
        }
    }

}

data class UpdateWikiConfigurationData(
        val accountId: Long,
        val channelId: String,
        val wikiURL: String,
        val wikiFilePath: String,
        val wikiUsername: String,
        val wikiPassword: String
)

data class UpdateWikiConfigurationResult(
        val wasSuccessful: Boolean,
        val responseText: String
)

data class CreateAccountData(
        val id: UUID,
        val channelId: String,
        val channelName: String
)

data class CreateAccountResult(
        val id: Long,
        val wasSuccessful: Boolean,
        val responseText: String
)

data class CreateDraftData(
        val id: UUID,
        val channelId: String
)

data class CreateDraftResult(
        val id: Long,
        val createdDraft: Draft,
        val wasSuccessful: Boolean,
        val responseText: String
)

data class AddDecisionData(
        val id: UUID,
        val text: String,
        val metadata: Metadata
)

data class AddDecisionResult(
        val id: UUID,
        val wasSuccessful: Boolean,
        val responseText: String
)

data class AddContextData(
        val id: UUID,
        val text: String,
        val slashCommand: SlashCommand
)

data class AddNeglectedOptionsData(
        val id: UUID,
        val text: String,
        val slashCommand: SlashCommand
)

data class AddQualityAttributesData(
        val id: UUID,
        val text: String,
        val slashCommand: SlashCommand
)

data class AddConcernsData(
        val id: UUID,
        val text: String,
        val slashCommand: SlashCommand
)

data class AddConsequencesData(
        val id: UUID,
        val text: String,
        val slashCommand: SlashCommand
)

data class SearchDecisionData(
        val id: UUID,
        val query: String,
        val metadata: Metadata
)

data class SearchDecisionResult(
        val id: UUID,
        val wasSuccessful: Boolean,
        val responseText: String
)

data class AbortResult(
        val wasSuccessful: Boolean,
        val responseText: String
)

data class ConfirmResult(
        val wasSuccessful: Boolean,
        val responseText: String
)