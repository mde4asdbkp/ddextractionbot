package nz.ac.canterbury.csse.seng402.slackdecisionmaker.modeloutput

import io.netty.channel.ChannelId
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.database.AccountRepository
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Account
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.EncryptionKey
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.WikiConfiguration
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.decrypt
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.ResetCommand
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import java.lang.Exception
import java.nio.file.Files

/**
 * Uses Git to output the given model to a location specified in the [WikiConfiguration]
 * retrieved based on the channel id of the decisions passed in.
 *
 * This allows different channels and organisations to use the same application backend.
 */
@Service
@Primary
class AuthorizedGitModelOutputter(
        private val accountRepository: AccountRepository,
        private val encryptionKey: EncryptionKey
) : ModelOutputter, DisposableBean {

    val logger: Logger = LoggerFactory.getLogger(GitModelOutputter::class.java)
    private val tempDir = Files.createTempDirectory(null)
    private lateinit var repo: Git
    private lateinit var wikiConfiguration: WikiConfiguration
    private lateinit var credentialsProvider: UsernamePasswordCredentialsProvider

    override fun outputModel(designDecisions: List<DesignDecision>) {
        val channelId: String = getChannelId(designDecisions)
        wikiConfiguration = obtainWikiConfiguration(channelId)

        credentialsProvider = UsernamePasswordCredentialsProvider(wikiConfiguration.username, wikiConfiguration.password)
        repo = Git.cloneRepository()
                .setURI(wikiConfiguration.url)
                .setCredentialsProvider(credentialsProvider)
                .setDirectory(tempDir.toFile())
                .call()

        val markdown = MarkdownParser.parseToMarkdown(designDecisions)
        uploadFile(markdown)
        destroy() // This slows things down but for the meantime is needed due to the possibility of different accounts
                  // using the same git repository

    }

    /**
     * Obtains the channelId based on the list of decisions given
     *
     * Can throw exception if not all of the channel ids match
     */
    fun getChannelId(designDecisions: List<DesignDecision>): String {
        val firstChannelId: String = designDecisions[0].metadata!!.channelId
        //TODO: Make this check more efficient
        for (decision in designDecisions) {
            if (decision.metadata!!.channelId != firstChannelId) {
                throw Exception("ChannelId was not the same for all decisions in the model!")
            }
        }
        return firstChannelId
    }

    /**
     * Obtains the correct wiki configuration associated with a channel
     */
    fun obtainWikiConfiguration(channelId: String): WikiConfiguration {

        val account: Account? = accountRepository.findByChannelId(channelId)

        var wikiUrl: String = ""
        var wikiUsername: String = ""
        var wikiPassword: String = ""
        var wikiRelativeFilePath: String = ""

        account?.let {
            wikiUrl = it.wikiURL!!
            wikiUsername = it.wikiUsername!!
            wikiRelativeFilePath = it.wikiFilePath!!
            wikiPassword = decrypt(it.wikiPassword!!, encryptionKey.key)
        }

        return WikiConfiguration(
                url = wikiUrl,
                username = wikiUsername,
                password = wikiPassword,
                relativeFilePath = wikiRelativeFilePath
        )
    }

    /**
     * Upload the given [newFileContents] to the file specified in the [WikiConfiguration] using Git.
     */
    fun uploadFile(newFileContents: String) {
        logger.debug("Beginning upload of file to remote repo")
        resetHead()
        val fileToEdit = tempDir.resolve(wikiConfiguration.relativeFilePath)
        Files.write(fileToEdit, newFileContents.toByteArray())
        repo.add().addFilepattern(wikiConfiguration.relativeFilePath).call()
        repo.commit().setMessage("Update Design Decisions").call()
        repo.push().setCredentialsProvider(credentialsProvider).call()
        logger.debug("Successful upload of file to remote repo")
    }

    private fun resetHead() {
        repo.fetch().setCredentialsProvider(credentialsProvider).setForceUpdate(true).call()
        repo.reset().setMode(ResetCommand.ResetType.HARD).call()
    }

    /**
     * Close the repo and remove the temp folder on shutdown.
     */
    override fun destroy() {
        logger.debug("Destroying resources")
        repo.close()
        tempDir.toFile().deleteRecursively()
        logger.debug("Successfully destroyed resources")
    }

}