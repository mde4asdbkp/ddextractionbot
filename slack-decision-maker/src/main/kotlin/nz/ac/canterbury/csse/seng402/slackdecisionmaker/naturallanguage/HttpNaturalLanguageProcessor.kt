package nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Context
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import java.util.*

/**
 * Interacts with another web service over HTTP to process natural language.
 */
@Service
class HttpNaturalLanguageProcessor (
        @Value("\${NLP_PORT:5000}") port: String,
        @Value("\${NLP_URL:http://127.0.0.1}") baseUrl: String,
        private val restTemplate: RestTemplate
) : NaturalLanguageProcessor {

    val logger: Logger = LoggerFactory.getLogger(HttpNaturalLanguageProcessor::class.java)

    private val rootUrl = "${baseUrl}:${port}"

    /**
     * Makes call to /process-add-message endpoint of natural language processor and handles response
     */
    override fun processAddMessage(message: String, id: UUID): DesignDecision {
        val responseEntity = try {
            logger.debug("$id: Making API call to NPL for message: $message")
            restTemplate.postForEntity(
                    "$rootUrl/process-add-message",
                    ProcessAddMessageBody(message, id),
                    DesignDecision::class.java
            )
        } catch (clientException: HttpClientErrorException) {
            logger.info("$id: Could not parse message, message was $message")
            logger.info("${clientException.message}")
            throw UnparsableMessageException("The message could not be parsed to a Design Decision. Please make sure you are conforming to the expected input.")
        } catch (serverException: HttpServerErrorException) {
            logger.error("$id: HttpServerException from the NLP API", serverException)
            throw Exception("Something went wrong!")
        }

        val responseBody = responseEntity.body
        if (responseEntity.statusCode != HttpStatus.OK || responseBody == null) {
            logger.error("$id: Non OK status code: ${responseEntity.statusCode} or null responseBody: $responseBody")
            throw Exception("Something went wrong!")
        } else {
            logger.debug("$id: Successful response from NLP API: $responseBody")
            return responseBody
        }
    }

    /**
     * Makes call to /process-context endpoint of natural language processor and handles response
     */
    override fun processContext(message: String, id: UUID): Context {
        val responseEntity = try {
            logger.debug("$id: Making API call to NLP for message: $message")
            restTemplate.postForEntity(
                    "$rootUrl/process-context",
                    ProcessAddMessageBody(message, id),
                    Context::class.java
            )
        } catch (clientException: HttpClientErrorException) {
            logger.info("$id: Could not parse message, message was $message")
            logger.info("${clientException.message}")
            throw UnparsableMessageException("The message could not be parsed to Context. Please make sure you are conforming to the expected input.")
        } catch (serverException: HttpServerErrorException) {
            logger.error("$id: HttpServerException from the NLP API", serverException)
            throw Exception("Something went wrong!")
        }

        val responseBody = responseEntity.body
        if (responseEntity.statusCode != HttpStatus.OK || responseBody == null) {
            logger.error("$id: Non OK status code: ${responseEntity.statusCode} or null responseBody: $responseBody")
            throw Exception("Something went wrong!")
        } else {
            logger.debug("$id: Successful response from NLP API: $responseBody")
            return responseBody
        }
    }

    /**
     * Makes call to /process-string-list endpoint of natural language processor and handles response
     */
    override fun processStringList(message: String, type: String, use_and: Boolean): List<String> {
        val id = UUID.randomUUID()
        val responseEntity = try {
            logger.debug("$id: Making API call to NLP for message: $message")
            restTemplate.postForEntity(
                    "$rootUrl/process-string-list",
                    ProcessStringListBody(id, message, type, use_and),
                    List::class.java
            )
        } catch (clientException: HttpClientErrorException) {
            logger.info("$id: Could not parse message, message was $message")
            logger.info("${clientException.message}")
            throw UnparsableMessageException("The message could not be parsed to a list. Please make sure you are conforming to the expected input.")
        } catch (serverException: HttpServerErrorException) {
            logger.error("$id: HttpServerException from the NLP API", serverException)
            throw Exception("Something went wrong!")
        }

        val responseBody = responseEntity.body
        if (responseEntity.statusCode != HttpStatus.OK || responseBody == null) {
            logger.error("$id: Non OK status code: ${responseEntity.statusCode} or null responseBody: $responseBody")
            throw Exception("Something went wrong!")
        } else {
            logger.debug("$id: Successful response from NLP API: $responseBody")
            return responseBody as List<String>
        }
    }


}

data class ProcessAddMessageBody(
        val message: String,
        val id: UUID
)

data class ProcessStringListBody(
        val id: UUID,
        val message: String,
        val type: String,
        val use_and: Boolean
)