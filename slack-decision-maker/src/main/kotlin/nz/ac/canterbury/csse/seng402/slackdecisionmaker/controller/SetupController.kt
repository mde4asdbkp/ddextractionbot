package nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.*
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlackResponse
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlashCommand
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.parseCommaSeparatedText
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.repository.config.RepositoryNameSpaceHandler
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.util.*
import kotlin.concurrent.thread


/**
 * The Slack endpoint to setup a user account and pass wiki configuration
 *
 * Takes tge [DomainModel] to trigger actions and a [RestTemplate] to POST back to Slack
 */
@RestController
class SetupController(
        private val domainModel: DomainModel,
        private val restTemplate: RestTemplate
) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SetupController::class.java)
        const val SETUP_ENDPOINT = "/setup"
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.createAccount] job,
     * followed by the [DomainModel.updateWikiConfiguration] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simply OK processing message to notify the Slack yser that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(SETUP_ENDPOINT)
    fun setupWikiConfiguration(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()
        logger.debug("$id: Setting up wiki configuration: $slashCommand")

        thread(start = true) {
            val wikiConfig: List<String> = parseCommaSeparatedText(slashCommand.text)
            val createAccountResult: CreateAccountResult? = domainModel.createAccount(CreateAccountData(
                    id,
                    slashCommand.channel_id,
                    slashCommand.channel_name
            ))

            val updateWikiConfigurationResult: UpdateWikiConfigurationResult = domainModel.updateWikiConfiguration(UpdateWikiConfigurationData(
                    accountId = createAccountResult!!.id,
                    channelId = slashCommand.channel_id,
                    wikiURL = wikiConfig[0],
                    wikiFilePath = wikiConfig[1],
                    wikiUsername = wikiConfig[2],
                    wikiPassword = wikiConfig[3]

            ))
            val wasSuccessful: Boolean = createAccountResult.wasSuccessful && updateWikiConfigurationResult.wasSuccessful
            val response = SlackResponse(
                    text = if (wasSuccessful) "Successfully setup account details" else "An error occurred when setting up account details!",
                    response_type = if (wasSuccessful) "in_channel" else "ephemeral"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)
        }

        return ResponseEntity.ok("Processing!")
    }

}