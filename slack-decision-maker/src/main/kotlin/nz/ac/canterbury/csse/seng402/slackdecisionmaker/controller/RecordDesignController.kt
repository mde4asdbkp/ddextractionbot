package nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.*
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Draft
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Metadata
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.User
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlackResponse
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.SlashCommand
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.draftCommandOutputText
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.time.Instant
import java.util.*
import kotlin.concurrent.thread

/**
 * The Slack endpoints to trigger design decisions.
 *
 * Takes the [DomainModel] to trigger actions and a [RestTemplate] to POST back to Slack
 */
@RestController
class RecordDesignController(
        private val domainModel: DomainModel,
        private val restTemplate: RestTemplate
) {

    companion object {

        val logger: Logger = LoggerFactory.getLogger(RecordDesignController::class.java)
        const val RECORD_ENDPOINT            = "/record-design"
        const val START_RECORDING_ENDPOINT   = "/start-recording"
        const val CONTEXT_ENDPOINT           = "/context"
        const val OPTION_ENDPOINT            = "/option"
        const val NEGLECTED_ENDPOINT         = "/neglected"
        const val QUALITY_ENDPOINT           = "/quality"
        const val CONCERN_ENDPOINT           = "/concern"
        const val CONSEQUENCE_ENDPOINT       = "/consequence"
        const val CONFIRM_RECORDING_ENDPOINT = "/confirm"
        const val ABORT_RECORDING_ENDPOINT   = "/abort"


    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.recordDecision] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(RECORD_ENDPOINT)
    fun recordDesign(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()
        logger.debug("$id: Recording design: $slashCommand")
        thread(start = true) {
            val recordingUser = User(slashCommand.user_name, slashCommand.user_id)
            val result = domainModel.recordDecision(AddDecisionData(
                    id,
                    slashCommand.text,
                    Metadata(
                            recordingUser,
                            setOf(recordingUser), //TODO: Include other users
                            Date.from(Instant.now()),
                            slashCommand.token,
                            slashCommand.channel_id
                    )
            ))
            val response = SlackResponse(
                    text = result.responseText,
                    response_type = if (result.wasSuccessful) "in_channel" else "ephemeral"
            )
            logger.debug("$id: Responding with result: $result, with response: $response")
            restTemplate.postForLocation(slashCommand.response_url, response)
        }
        logger.debug("$id: Replying with initial processing message for ${slashCommand.response_url}")
        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.createDraft] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(START_RECORDING_ENDPOINT)
    fun startRecording(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            // Create entry in draft table if one does not already exist

            val createDraftResult: CreateDraftResult? = domainModel.createDraft(CreateDraftData(
                    id = id,
                    channelId = slashCommand.channel_id
            ))

            //Clear draft table entry if already exists and for some reason not clear

            domainModel.abort(slashCommand)

            // Respond with user instructions based on what is not filled in (create reusable component for other methods to use)

            var responseText = ""

            if (createDraftResult!!.wasSuccessful) {
                responseText = "Successfully began recording new draft design decision!" + "\n\n" +
                        draftCommandOutputText(createDraftResult.createdDraft)
            } else {
                responseText = "Something went wrong!"
            }

            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)
        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.addContext] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(CONTEXT_ENDPOINT)
    fun addContext(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val updatedDraft: Draft? = domainModel.addContext(AddContextData(
                    id = id,
                    text = slashCommand.text,
                    slashCommand = slashCommand
            ))

            val responseText = if (updatedDraft != null) "Successfully added context, use the command again to update\\nn" +
                    draftCommandOutputText(updatedDraft) else "Something went wrong!"

            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.addOption] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(OPTION_ENDPOINT)
    fun addOption(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val updatedDraft: Draft = domainModel.addOption(slashCommand)

            val responseText = "Successfully added chosen option, use the command again to update\n\n" +
                    draftCommandOutputText(updatedDraft)


            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.addNeglectedOptions] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(NEGLECTED_ENDPOINT)
    fun addNeglectedOptions(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val updatedDraft: Draft? = domainModel.addNeglectedOptions(AddNeglectedOptionsData(
                    id = id,
                    text = slashCommand.text,
                    slashCommand = slashCommand
            ))

            val responseText = if (updatedDraft != null) "Successfully added neglected options, use the command again to update\n\n" +
                    draftCommandOutputText(updatedDraft) else "Something went wrong!"

            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.addQualityAttributes] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(QUALITY_ENDPOINT)
    fun addQualityAttributes(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val updatedDraft: Draft? = domainModel.addQualityAttributes(AddQualityAttributesData(
                    id = id,
                    text = slashCommand.text,
                    slashCommand = slashCommand
            ))

            val responseText = if (updatedDraft != null) "Successfully added quality attributes, use the command again to update\n\n" +
                    draftCommandOutputText(updatedDraft) else "Something went wrong!"

            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.addConcerns] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(CONCERN_ENDPOINT)
    fun addConcerns(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val updatedDraft: Draft? = domainModel.addConcerns(AddConcernsData(
                    id = id,
                    text = slashCommand.text,
                    slashCommand = slashCommand
            ))

            val responseText = if (updatedDraft != null) "Successfully added concerns, use the command again to update\n\n" +
                    draftCommandOutputText(updatedDraft) else "Something went wrong!"

            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.addConsequences] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(CONSEQUENCE_ENDPOINT)
    fun addConsequences(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val updatedDraft: Draft? = domainModel.addConsequences(AddConsequencesData(
                    id = id,
                    text = slashCommand.text,
                    slashCommand = slashCommand
            ))

            val responseText = if (updatedDraft != null) "Successfully added consequences, use the command again to update\n\n" +
                    draftCommandOutputText(updatedDraft) else "Something went wrong!"

            val response = SlackResponse(
                    text = responseText,
                    response_type = "in_channel"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.confirm] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(CONFIRM_RECORDING_ENDPOINT)
    fun confirm(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val confirmResult: ConfirmResult = domainModel.confirm(slashCommand)

            val response = SlackResponse(
                    text = confirmResult.responseText,
                    response_type = if (confirmResult.wasSuccessful) "in_channel" else "ephemeral"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

    /**
     * Accept a POST request with body conforming to the Slack slash command API in the form of the [SlashCommand] data class
     *
     * Works asynchronously, initially creating the [DomainModel.abort] job,
     * which upon result, POSTs to the Slack API to notify the user.
     *
     * Initially returns a simple OK processing message to notify the Slack user that the request is in progress.
     *
     * @see <a href="https://api.slack.com/slash-commands">Slack Slash Command API</a>
     */
    @PostMapping(ABORT_RECORDING_ENDPOINT)
    fun abort(slashCommand: SlashCommand): ResponseEntity<String> {
        val id = UUID.randomUUID()

        thread(start = true) {

            val abortResult: AbortResult = domainModel.abort(slashCommand)

            val response = SlackResponse(
                    text = abortResult.responseText,
                    response_type = if (abortResult.wasSuccessful) "in_channel" else "ephemeral"
            )
            restTemplate.postForLocation(slashCommand.response_url, response)

        }

        return ResponseEntity.ok("Processing!")
    }

}