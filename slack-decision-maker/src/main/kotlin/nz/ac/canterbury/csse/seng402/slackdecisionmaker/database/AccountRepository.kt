package nz.ac.canterbury.csse.seng402.slackdecisionmaker.database

import io.netty.channel.ChannelId
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller.SearchDatabaseController
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Account
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.NoResultException
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root
import javax.transaction.Transactional

/**
 * Custom Account repository interface allowing extensions of simple [CrudRepository]
 */
interface AccountRepositoryExtended {
    fun persistNewAccountIfNoneExistsWithChannelId(@Param("account") account: Account): Account

    fun findByChannelId(@Param("channelId") channelId: String): Account?
}

/**
 * Simple Spring [CrudRepository] for Accounts. Auto generates everything based on [javax.persistence] annotations
 */
@Repository
interface AccountRepository: CrudRepository<Account, Long>, AccountRepositoryExtended

/**
 * Implementation of custom Account repository methods
 */
open class AccountRepositoryImpl(private val entityManager: EntityManager): AccountRepositoryExtended {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SearchDatabaseController::class.java)
    }

    /**
     * Take an Account model and persist it if one does not already exist with the associated channel id
     *
     * Returns either the existing account, or newly created account.
     */
    @Transactional
    override fun persistNewAccountIfNoneExistsWithChannelId(account: Account): Account {

        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val query: CriteriaQuery<Account> = criteriaBuilder.createQuery(Account::class.java)
        val accountRoot: Root<Account> = query.from(Account::class.java)

        query.select(accountRoot).where(
                criteriaBuilder.equal(accountRoot.get<String>("channelId"), account.channelId)
        )

        return try {
            entityManager.createQuery(query).singleResult
        } catch (e: NoResultException) {
            entityManager.merge(account)
        }
    }

    /**
     * Returns an account associated with a given channel id, or null if no such account exists
     */
    override fun findByChannelId(channelId: String): Account? {
        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val query: CriteriaQuery<Account> = criteriaBuilder.createQuery(Account::class.java)
        val accountRoot: Root<Account> = query.from(Account::class.java)

        query.select(accountRoot).where(
                criteriaBuilder.equal(accountRoot.get<String>("channelId"), channelId)
        )

        return try {
            entityManager.createQuery(query).singleResult
        } catch (e: NoResultException) {
            null
        }
    }

}