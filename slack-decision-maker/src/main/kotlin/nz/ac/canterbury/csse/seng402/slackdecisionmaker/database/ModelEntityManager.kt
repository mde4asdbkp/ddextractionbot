package nz.ac.canterbury.csse.seng402.slackdecisionmaker.database

import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.transaction.Transactional

@Service
class ModelEntityManager(
        private val entityManager: EntityManager
) {

    /**
     * Persist an entity, handling nested entities that may already exist in the database
     */
    @Transactional
    fun <E> persistSafely(entity: E): E {
        return entityManager.merge(entity)
    }

}