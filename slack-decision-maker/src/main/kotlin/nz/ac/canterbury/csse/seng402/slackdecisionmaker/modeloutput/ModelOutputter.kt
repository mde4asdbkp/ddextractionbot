package nz.ac.canterbury.csse.seng402.slackdecisionmaker.modeloutput

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision

/**
 * Accepts a given [DesignDecision] list and outputs it to the location configured.
 */
interface ModelOutputter {

    /**
     * Outputs the given [designDecisions] to an external user facing location.
     */
    fun outputModel(designDecisions: List<DesignDecision>)

}