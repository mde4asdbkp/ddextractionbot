package nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Standard "healthcheck" endpoint that will just return OK to verify the server is running.
 * Does not provide any information on the usability of the server (DB connection, etc.)
 */
@RestController
class HealthcheckController {

    @GetMapping("/healthcheck")
    fun healthcheck(): String {
        return "OK"
    }
}