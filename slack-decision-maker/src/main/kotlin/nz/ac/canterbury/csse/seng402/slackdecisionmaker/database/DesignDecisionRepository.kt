package nz.ac.canterbury.csse.seng402.slackdecisionmaker.database

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.controller.SearchDatabaseController
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Metadata
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.parseCommaSeparatedText
import org.hibernate.criterion.Restrictions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.TypedQuery
import javax.persistence.criteria.*
import javax.persistence.metamodel.EntityType
import javax.transaction.Transactional

/**
 * Custom Design Decision Repository Interface allowing extension of simple [CrudRepository]
 */
interface DesignDecisionRepositoryExtended {
//    @Query("select d from DesignDecision d where d.inputText like %:queryString%")
    fun findDecisionsByQueryString(@Param("queryString") queryString: String, @Param("channelId") channelId: String): List<DesignDecision>

    fun findDecisionsByChannelId(@Param("channelId") channelId: String): List<DesignDecision>
}

/**
 * Simple Spring [CrudRepository] for DesignDecisions. Auto generates everything based on [javax.persistence] annotations
 */
@Repository
interface DesignDecisionRepository: CrudRepository<DesignDecision, Long>, DesignDecisionRepositoryExtended

/**
 * Implementation of custom Design Decision Repository methods
 */
open class DesignDecisionRepositoryImpl(private val entityManager: EntityManager) : DesignDecisionRepositoryExtended {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(SearchDatabaseController::class.java)
    }

    /**
     * Return a list of decisions that match the keywords passed as comma separated values in the query string.
     * If no keywords are passed, all decisions for the given account (channelId) are returned.
     */
    override fun findDecisionsByQueryString(queryString: String, channelId: String): List<DesignDecision> {

        var queries = parseCommaSeparatedText(queryString)

        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        var query: CriteriaQuery<DesignDecision> = criteriaBuilder.createQuery(DesignDecision::class.java)
        var decisionRoot: Root<DesignDecision> = query.from(DesignDecision::class.java)
        var likes = criteriaBuilder.like(criteriaBuilder.lower(decisionRoot.get<String>("inputText")), "%" + queries[0].toLowerCase() + "%")

        for (q in queries) {
            likes = criteriaBuilder.or(likes, criteriaBuilder.like(criteriaBuilder.lower(decisionRoot.get<String>("inputText")), "%" + q.toLowerCase() + "%"))
        }
        query.
            select(decisionRoot).
            where(criteriaBuilder.and(
                    criteriaBuilder.equal(decisionRoot.get<Metadata>("metadata").get<String>("channelId"), channelId),
                    likes
                )
            )

        return entityManager.createQuery(query).resultList
    }

    /**
     * Returns a list of decisions associated with a given channelId
     */
    override fun findDecisionsByChannelId(channelId: String): List<DesignDecision> {
        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        var query: CriteriaQuery<DesignDecision> = criteriaBuilder.createQuery(DesignDecision::class.java)
        var decisionRoot: Root<DesignDecision> = query.from(DesignDecision::class.java)
        query.select(decisionRoot).where(
                criteriaBuilder.equal(decisionRoot.get<Metadata>("metadata").get<String>("channelId"), channelId)
        )

        return entityManager.createQuery(query).resultList
    }

}