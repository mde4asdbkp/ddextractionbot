package nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel

import javax.persistence.*

/**
 * The data model for an Account in the system.
 *
 * Contains [javax.persistence] annotations to allow Spring JPA to persist data.
 */
@Entity
data class Account(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val channelId: String,

        var channelName: String,

        var wikiURL: String? = null,

        var wikiFilePath: String? = null,

        var wikiUsername: String? = null,

        var wikiPassword: String? = null

)