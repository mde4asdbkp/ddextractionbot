package nz.ac.canterbury.csse.seng402.slackdecisionmaker.modeloutput

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.util.WikiConfiguration
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.ResetCommand
import org.eclipse.jgit.transport.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.stereotype.Service
import java.nio.file.Files

/**
 * Uses Git to output the given model to the location specified in the [WikiConfiguration]
 */
@Service
class GitModelOutputter(
        private val wikiConfiguration: WikiConfiguration
) : ModelOutputter, DisposableBean {

    val logger: Logger = LoggerFactory.getLogger(GitModelOutputter::class.java)

    private val tempDir = Files.createTempDirectory(null)

    private val credentialsProvider = UsernamePasswordCredentialsProvider(wikiConfiguration.username, wikiConfiguration.password)

    private val repo = Git.cloneRepository()
            .setURI(wikiConfiguration.url)
            .setCredentialsProvider(credentialsProvider)
            .setDirectory(tempDir.toFile())
            .call()

    override fun outputModel(designDecisions: List<DesignDecision>) {
        val markdown = MarkdownParser.parseToMarkdown(designDecisions)
        uploadFile(markdown)
    }

    /**
     * Upload the given [newFileContents] to the file specified in the [WikiConfiguration] using Git.
     */
    fun uploadFile(newFileContents: String) {
        logger.debug("Beginning upload of file to remote repo")
        resetHead()
        val fileToEdit = tempDir.resolve(wikiConfiguration.relativeFilePath)
        Files.write(fileToEdit, newFileContents.toByteArray())
        repo.add().addFilepattern(wikiConfiguration.relativeFilePath).call()
        repo.commit().setMessage("Update Design Decisions").call()
        repo.push().setCredentialsProvider(credentialsProvider).call()
        logger.debug("Successful upload of file to remote repo")
    }

    private fun resetHead() {
        repo.fetch().setCredentialsProvider(credentialsProvider).setForceUpdate(true).call()
        repo.reset().setMode(ResetCommand.ResetType.HARD).call()
    }

    /**
     * Close the repo and remove the temp folder on shutdown.
     */
    override fun destroy() {
        logger.debug("Destroying resources")
        repo.close()
        tempDir.toFile().deleteRecursively()
        logger.debug("Successfully destroyed resources")
    }

}