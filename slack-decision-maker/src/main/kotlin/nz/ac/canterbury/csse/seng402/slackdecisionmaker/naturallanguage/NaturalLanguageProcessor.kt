package nz.ac.canterbury.csse.seng402.slackdecisionmaker.naturallanguage

import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.Context
import nz.ac.canterbury.csse.seng402.slackdecisionmaker.metamodel.DesignDecision
import java.util.*

/**
 * Accepts natural language and converts it to the given model.
 */
interface NaturalLanguageProcessor {

    /**
     * Take a given [message] and convert it to the [DesignDecision] model.
     *
     * @throws UnparsableMessageException When the given [message] cannot be converted to a [DesignDecision]
     */
    fun processAddMessage(message: String, id:UUID): DesignDecision

    /**
     * Take a given [message] and convert it to the [Context] model.
     *
     * @throws UnparsableMessageException When the given [message] cannot be converted to a [Context]
     */
    fun processContext(message: String, id: UUID): Context

    /**
     * Take a given [message] and convert it to a list of strings.
     *
     * @throws UnparsableMessageException When the given [message] cannot be converted to a list of strings
     */
    fun processStringList(message: String, type: String, use_and: Boolean): List<String>

}

class UnparsableMessageException(message: String): Exception(message)