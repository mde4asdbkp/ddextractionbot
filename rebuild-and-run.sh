#!/bin/bash
set -e

./slack-decision-maker/gradlew -p ./slack-decision-maker build
docker-compose build
docker-compose up