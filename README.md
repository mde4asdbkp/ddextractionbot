# README

## Overview

The ddextractionbot is an automated bot for recording design decisions directly from Slack. The purpose of this document is to explain the following, in distinct subsections:

* Providing configuration information
* Running existing system locally
* Setting up in a Slack workspace
* Usage information
* Further development

*It should be noted that these are all in the context of developers. Running and using the application as a real software team should be as simple as adding a published Slack App to their workspace, and using the appropriate commands to setup their account and link to their team's wiki.*

## Configuring

In a previous version of the solution, all configuration when running the containerised solution was provided with a .env file.

In the current system, all configuration information needs to be provided in an application.yml file, that gets placed into ```ddextractionbot/slack-decision-maker/src/main/resources```
An example of what this file needs to look like can be found at the top of the repo.

A .env file can be used to specify the follwing when using docker-compose:

* MAIN_PORT=8080
* NLP_PORT=8081
* FLASK_RUN_PORT=8081

## Running

Having provided the configuration information using the method above, simply run the docker-compose file to start a web server running all the dependencies using:

```
docker-compose up
```

Note: this requires docker and docker-compose to be installed on the system running these commands.

Alternatively, run the simple script to rebuild the dependencies and then start the docker containers. Note, this does require Java 11 JDK. This will be required if the configuration has changed.

```
./rebuild-and-run.sh
```

Note that running the system in a production sense is currently done in an identical fashion, more information about this can be found [here](https://bitbucket.org/mde4asd/ddextractionbot/wiki/Hosting%20and%20Deployment)

## Slack Setup

As mentioned above, as an end user, a software team should just be able to add a published app to their Slack workspace. If this app has not been published and needs to be set up, follow the instructions below.

To link the bot to an existing Slack channel, first create a Slack App at:

https://api.slack.com/apps

Once an app has been created, create the following Slash Commands, where ```url``` is the address where the system is hosted. As mentioned below, when developing locally this will likely be through ```ngrok``` and look something like `http://e6ef4ad7.ngrok.io/record-design`. If hosting elsewhere, such as the process outlined [here](https://bitbucket.org/mde4asd/ddextractionbot/wiki/Hosting%20and%20Deployment), it may look like  `http://ec2-12-345-678-910.compute-1.amazonaws.com:8080/record-design`

* `http://url/record-decision`
* `http://url/search-decisions`
* `http://url/start-recording`
* `http://url/setup`
* `http://url/confirm`
* `http://url/abort`
* `http://url/context`
* `http://url/option`
* `http://url/neglected`
* `http://url/quality`
* `http://url/concern`
* `http://url/consequence`


Install the Slack App to your workspace and use it by triggering the defined commands. Explanations of these commands can be found in the following section.

## Usage

### Explanation

The ddextraction bot allows multiple software teams / projects to record decisions within the same slack workspace, using the same attached bot. This is achieved by having multiple accounts. In this iteration of the bot, a Slack channel is considered synonymous to an account. Once the bot has been installed into the workspace, the /setup command needs to be run for each channel. This will tell the bot where to store decisions made in that channel, and what decisions to retrieve when searching. Having set up the channel, the below commands can be used to interactively create a draft design decision, and then persist it, or abort. As an account is considered to be a channel, as opposed to a slack user, multiple team members can contribute different parts of the decisions before persisting it.

### Commands

* `/record-decision`: Use this command and pass in text following the y-model format to use the first generation decision recording functionality.
* `/search-decisions`: Pass this command as is, or with comma-separated keywords to search for decisions attached to the current channel
* `/setup`: Use this command to pass in wiki configuration for the account/channel. Values should be passed in as comma-separated values. /setup wiki-url, wiki-file-path, wiki-username, wiki-password Example:
```
/setup https://x-token-auth:password@bitbucket.org/mde4asd/ddextractionbot.git/wiki, DesignDecisionsSamTesting.md, sam-annand, password
``` 
The example above uses token authorization, for which the token must also be passed into the wiki url as seen above.

If using bitbucket, the token can be generated as an app password under account settings.

* `/start-recording`: Use this command with no arguments to start recording a chat based design decision. Technically, this only needs to be done once ever in each channel / account.
* `/context`: Pass context for the decision. Can either be just a use case, or if separated with a comma or an 'and', will be use case and component. **[Required]**
* `/option`: Pass the single, chosen option **[Required]**
* `/neglected`: Pass one or more neglected options. Can be separated with a comma or 'and''s
* `/quality`: Pass one or more quality attributes. Can be separated with a comma or 'and''s
* `/concern`: Pass one or more concerns. Can be separated with a comma. Will not separate on 'and'.
* `/consequence`: Pass one or more consequences. Can be separated with a comma. Will not separate on 'and'.
* `/confirm`: Confirm the draft design decision, persist it to the database, publish it to associated wiki.
* `/abort`: Abort the current draft.

## Developing

### Requirements

* Java 11 JDK - OpenJDK fine
* Python 3
* Docker
* Docker Compose

### Setup and configuration

To provide database configuration while developing, copy the `application.yml.example` to `application.yml` inside the slack-decision-maker folder, and fill in the fields. This is the same as when running in a 'production' sense as mentioned above. See the associated issue on the issue tracker to fix this.

The slack-decision-maker app will look for the natural-language-processor on its default port, so simply running both the Kotlin and Python projects in the same directory will be sufficient. We reccommend using IntelliJ IDEA for development of the slack-decision-maker, and PyCharm for development of the natural-language-processor

All other dependency management is specified in the `requirements.txt` for the natural-language-processor, or the `build.gradle.kts` file for slack-decision-maker.

For the natural language processor, I found the model listed in requirements was causing issues. Having installed the other dependencies, I found this command helpful:
```
python3 -m spacy download en_core_web_sm
```

Local development can be enabled simply by using `ngrok`. Install it to the local development machine, and then run `ngrok http 8080` to create a tunnel to your local machine. Select the `Forwarding` internet address and configure the Slack slash commands to point to that URL and the /record-design endpoint, for example `http://e6ef4ad7.ngrok.io/record-design`. Slash commands will then be sent directly to the local development system.