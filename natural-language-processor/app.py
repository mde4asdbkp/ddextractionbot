"""
Main app.py file for server. Contains API entry points
"""

from flask import Flask, request
from DesignDecisionParser import InvalidDesignDecisionTextError, DesignDecisionParser

app = Flask(__name__)
design_decision_parser = DesignDecisionParser()


@app.route('/process-add-message', methods=["POST"])
def hello_world():
    """
    Entry point for processing an entire piece of text into a design decision following the y-model.
    Main functionality from first project iteration. No reason for function to be called hello_world(),
    but have left as is for good luck.
    :return: HTTP Response
    """

    body = request.json
    uid = body["id"]
    print(f"{uid} Processing request with body: {body}")
    try:
        app.logger.info(body["message"])
        result = design_decision_parser.text_to_design_decision(body["message"])
        print(f"{uid} Successfully processed request with result: {result.to_json()}")
        return app.response_class(response=result.to_json(), status=200, mimetype='application/json')
    except InvalidDesignDecisionTextError as e:
        print(f"{e}")
        print(f"{uid} Request could not be parsed to a DesignDecision")
        return app.response_class(response="Given message does not conform to the DesignDecision format", status=400)
    except Exception as e:
        print(f"{uid} Error when processing request: {e}")
        return app.response_class(response="Internal server error", status=500)


@app.route('/process-context', methods=["POST"])
def process_context():
    """
    Entry point for processing a single context from a given piece of text
    :return: HTTP response
    """

    body = request.json
    uid = body["id"]
    print(f"{uid} Processing request with body: {body}")
    try:
        app.logger.info(body["message"])
        result = design_decision_parser.text_to_context(body["message"])
        print(f"{uid} Successfully processed request with result: {result.to_json()}")
        return app.response_class(response=result.to_json(), status=200, mimetype='application/json')
    except InvalidDesignDecisionTextError as e:
        print(f"{e}")
        print(f"{uid} Request could not be parsed to Context")
        return app.response_class(response="Given message does not conform to the Context format", status=400)
    except Exception as e:
        print(f"{uid} Error when processing request: {e}")
        return app.response_class(response="Internal server error", status=500)


@app.route('/process-string-list', methods=["POST"])
def process_string_list():
    """
    Entry point for processing a text based list, that uses either commands or conjunctions as separators, into a list
    :return: HTTP Response
    """

    body = request.json
    uid = body["id"]
    print(f"{uid} Processing request with body: {body}")
    try:
        string_list = body["message"]
        list_type = body["type"]
        use_and = body["use_and"]
        result = design_decision_parser.text_to_type_list(string_list, list_type, use_and)
        print(f"{uid} Successfully processed request with result: {result}")
        return app.response_class(response=result, status=200, mimetype='application/json')
    except Exception as e:
        print(f"{uid} Error when processing request: {e}")
        return app.response_class(response="Internal server error", status=500)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
