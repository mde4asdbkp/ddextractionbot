"""
DesignDecisionParser

Code for parsing design decisions, contexts, and string lists. Contains 'dumb' implementation with simple Python methods
as well as NLP based solution (which is what is actually used)
"""

import json
import re
from dataclasses import dataclass
from typing import Optional, List, Tuple, Dict
import spacy
from spacy.matcher import Matcher


@dataclass
class Context:
    """Class representing the Context of the Design Decision Meta Model"""

    useCase: str
    component: Optional[str]

    def to_json(self) -> str:
        """Converts Context class to JSON"""

        return json.dumps(self.__dict__, default=lambda x: x.__dict__)

    @classmethod
    def from_json(cls, json_str: str) -> 'Context':
        """Converts JSON context into Context class"""

        json_dict = json.loads(json_str)
        return cls(**json_dict)


@dataclass
class NonFunctionalConcern:
    """Class representing the Concern of the Design Decision Meta Model"""

    concern: str


@dataclass
class Option:
    """Class representing the Option of the Design Decision Meta Model"""

    name: str

    def to_json(self) -> str:
        """Converts Option class to JSON"""

        return json.dumps(self.__dict__, default=lambda x: x.__dict__)

    @classmethod
    def from_json(cls, json_str: str) -> 'Option':
        """Converts JSON option into Option class"""

        json_dict = json.loads(json_str)
        return cls(**json_dict)


@dataclass
class QualityAttribute:
    """Class representing the Quality of the Design Decision Meta Model"""

    attribute: str


@dataclass
class Consequence:
    """Class representing the Consequence of the Design Decision Meta Model"""

    consequence: str


# noinspection PyPep8Naming
class DesignDecision:
    """Class representing the Design Decision of the Design Decision Meta Model"""

    context: Context
    nonFunctionalConcernsAddressed: Optional[List[NonFunctionalConcern]]
    chosenOption = Option
    neglectedOptions: List[Option]
    qualityAttributesAchieved: Optional[List[QualityAttribute]]
    consequences: Optional[List[Consequence]]

    inputText: str

    def __init__(self,
                 context,
                 nonFunctionalConcernsAddressed,
                 chosenOption,
                 neglectedOptions,
                 qualityAttributesAchieved,
                 consequences,
                 inputText) -> None:
        self.context = context
        self.nonFunctionalConcernsAddressed = nonFunctionalConcernsAddressed
        self.chosenOption = chosenOption
        self.neglectedOptions = neglectedOptions
        self.qualityAttributesAchieved = qualityAttributesAchieved
        self.consequences = consequences
        self.inputText = inputText

    def to_json(self) -> str:
        """Converts DesignDecision class to JSON"""

        return json.dumps(self.__dict__, default=lambda x: x.__dict__)

    @classmethod
    def from_json(cls, json_str: str) -> 'DesignDecision':
        """Converts JSON context into DesignDecision class"""

        json_dict = json.loads(json_str)
        return cls(**json_dict)


class DumbDesignDecisionParser:
    """Simple design decision parsing using basic Python methods"""

    @staticmethod
    def text_to_design_decision_dumb(input_str: str) -> DesignDecision:
        """Attempts to convert plain text (in y-model format) into a DesignDecision"""

        try:
            input_str = input_str.strip()
            input_str = input_str.rstrip(".")

            context = DumbDesignDecisionParser._between(input_str, "In the context of ", ", facing non-functional")
            context = Context(context.strip(), None)

            nfrs = DumbDesignDecisionParser._between(input_str, ", facing non-functional concerns ", ", we chose")
            nfrs = re.split(" and |, ", nfrs)
            nfrs = list(map(lambda x: NonFunctionalConcern(x.strip()), nfrs))

            chosen_option = DumbDesignDecisionParser._between(input_str, "we chose ", ", and neglected")
            chosen_option = Option(chosen_option.strip())

            neglected_options = DumbDesignDecisionParser._between(input_str, ", and neglected ", ", to achieve")
            neglected_options = re.split(" and |, ", neglected_options)
            neglected_options = list(map(lambda x: Option(x.strip()), neglected_options))

            qualities = DumbDesignDecisionParser._between(input_str, ", to achieve ", ", accepting")
            qualities = re.split(" and |, ", qualities)
            qualities = list(map(lambda x: QualityAttribute(x.strip()), qualities))

            consequences = input_str.split(", accepting ")[1]
            consequences = re.split(" and |, ", consequences)
            consequences = list(map(lambda x: Consequence(x.strip()), consequences))

            return DesignDecision(context, nfrs, chosen_option, neglected_options, qualities, consequences, input_str)
        except IndexError:
            raise InvalidDesignDecisionTextError()

    @staticmethod
    def _between(input_str: str, start: str, end: str) -> str:
        """Helper method to extract the text between the start string, and the end string, in the input string"""

        return input_str.split(start)[1].split(end)[0]


class DesignDecisionParser:
    """Natural Language Processing based implementation of Design Decision parsing using Spacy"""

    def __init__(self) -> None:
        self.nlp = spacy.load("en_core_web_sm")

    def get_matches(self, doc, patterns: List[List[Dict]]) -> Tuple[int, int]:
        matcher = Matcher(self.nlp.vocab)
        for pattern in patterns:
            matcher.add("Unused", None, pattern)
        match = max(matcher(doc), key=lambda m: m[2] - m[1])
        return match[1], match[2]

    def get_strings_separated(self, string, single_arg_constructor, separator_regex=" and |, "):
        """Return a list of the given class, separated in the given string by the given separator"""

        results = string.strip()
        results = re.split(separator_regex, results)
        results = filter(None, map(lambda s: s.strip(), results))
        results = list(map(lambda s: single_arg_constructor(s), results))
        return results

    def get_strings_separated_no_class(self, string, single_arg_constructor, separator_regex=" and |, "):
        """Return a list, separated in the given string by the given separator"""

        results = string.strip()
        results = re.split(separator_regex, results)
        results = filter(None, map(lambda s: s.strip(), results))
        results = list(results)
        return results

    def text_to_context(self, input_str: str) -> Context:
        """Convert plain text matching the format expected from a /context command, into a Context class"""

        try:
            input_str = input_str.strip()
            input_str = input_str.rstrip(".")

            input_str = re.split(" and |, ", input_str)

            if len(input_str) > 1:
                context = Context(input_str[0], input_str[1])
            elif len(input_str[0]) == 0:
                raise InvalidDesignDecisionTextError()
            else:
                context = Context(input_str[0], None)

            return context

        except IndexError:
            raise InvalidDesignDecisionTextError()

    def text_to_design_decision(self, input_str: str) -> DesignDecision:
        """Attempts to convert plain text (in y-model format) into a DesignDecision"""

        try:
            input_str = input_str.strip()
            input_str = input_str.rstrip(".")

            doc = self.nlp(input_str)

            _, context_start = self.get_matches(doc, [
                [{"LEMMA": "in"}, {"LEMMA": "the"}, {"LEMMA": "context"}, {"LEMMA": "of"}]
            ])

            context_end, nfc_start = self.get_matches(doc, [
                [{"IS_PUNCT": True, "OP": "?"}, {"LOWER": "facing"}, {"LOWER": "non"}, {"IS_PUNCT": True, "OP": "?"},
                 {"LOWER": "functional"}, {"LEMMA": {"IN": ["concern", "concerns"]}}, {"LEMMA": "that", "OP": "?"}],
                [{"IS_PUNCT": True, "OP": "?"}, {"LOWER": "facing"}, {"LOWER": "nonfunctional", "OP": "?"},
                 {"LEMMA": {"IN": ["concern", "concerns"]}}, {"LEMMA": "that", "OP": "?"}],
                [{"IS_PUNCT": True, "OP": "?"}, {"LOWER": "facing"}, {"LEMMA": "that", "OP": "?"}]
            ])

            nfc_end, chosen_opt_start = self.get_matches(doc, [
                [{"IS_PUNCT": True, "OP": "?"}, {"POS": "PRON"}, {"LEMMA": "choose"}],
                [{"IS_PUNCT": True, "OP": "?"}, {"POS": "PRON"}, {"LEMMA": "decide"}, {"LOWER": "to", "OP": "?"}],
                [{"IS_PUNCT": True, "OP": "?"}, {"POS": "PRON"}, {"LEMMA": "decide"}, {"LOWER": "to"}, {"LEMMA": "use"}]
            ])

            try:
                chosen_opt_end, neg_opts_start = self.get_matches(doc, [
                    [{"IS_PUNCT": True, "OP": "?"}, {"LEMMA": "and", "OP": "?"}, {"LEMMA": "neglect"}]
                ])

                neg_opts_end, quality_start = self.get_matches(doc, [
                    [{"IS_PUNCT": True, "OP": "?"}, {"LEMMA": "to"}, {"LEMMA": "achieve"}]
                ])
            except ValueError:
                neg_opts_start = neg_opts_end = 0
                chosen_opt_end, quality_start = self.get_matches(doc, [
                    [{"IS_PUNCT": True, "OP": "?"}, {"LEMMA": "to"}, {"LEMMA": "achieve"}]
                ])

            quality_end, consequences_start = self.get_matches(doc, [
                [{"IS_PUNCT": True, "OP": "?"}, {"LEMMA": "accept"}, {"LEMMA": "that", "OP": "?"}]
            ])

            consequences_end = len(doc)

            context = doc[context_start:context_end].string.strip()
            context = Context(context, None)

            nfcs = self.get_strings_separated(doc[nfc_start:nfc_end].string, NonFunctionalConcern)

            chosen_option = doc[chosen_opt_start:chosen_opt_end].string.strip()
            chosen_option = Option(chosen_option)

            neglected_options = self.get_strings_separated(doc[neg_opts_start:neg_opts_end].string, Option)

            qualities = self.get_strings_separated(doc[quality_start:quality_end].string, QualityAttribute)

            consequences = self.get_strings_separated(doc[consequences_start:consequences_end].string, Consequence)

            return DesignDecision(context, nfcs, chosen_option, neglected_options, qualities, consequences, input_str)
        except IndexError as e:

            raise InvalidDesignDecisionTextError()
        except ValueError:
            raise InvalidDesignDecisionTextError()

    def text_to_type_list(self, string_list, list_type, use_and):
        """
        Convert text in a list format to a list
        :use_and: Whether or not to separate the list on 'and'
        TODO: Currently types are not used, this should be updated
        """

        type_dict = {
            'consequence': Consequence,
            'concern': NonFunctionalConcern,
            'quality': QualityAttribute,
            'neglected': Option
        }

        if use_and:
            regex = " and |, "
        else:
            regex = ", "

        return json.dumps(self.get_strings_separated_no_class(string_list, type_dict[list_type], separator_regex=regex))


class InvalidDesignDecisionTextError(Exception):
    pass
