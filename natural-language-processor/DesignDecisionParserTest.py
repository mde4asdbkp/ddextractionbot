"""
Acceptance testing of DesignDecisionParser

TODO: Add tests for new and updated functionality related to chat-based implementation
"""

import unittest
import DesignDecisionParser


class MyTestCase(unittest.TestCase):
    sut = DesignDecisionParser.DesignDecisionParser()

    def test_standard_caps_does_return_expected_model(self):
        # Given
        test_text = "In the context of The add users use case, facing non-functional concerns speed and reliability, " \
                    "we chose a relational database, and neglected an in memory datastore and a NoSQL database, " \
                    "to achieve rapid lookups and schema validity, accepting the extra effort required and the need " \
                    "for vertical scaling."
        # When
        design_decision = self.sut.text_to_design_decision(test_text)
        # Then
        self.verify_standard_decision(design_decision)

    def test_differing_structure_in_defined_parts_does_return_expected_model(self):
        # Given
        test_text = "in the context of The add users use case, facing nonfunctional concern speed and reliability " \
                    "I chose a relational database and neglected an in memory datastore and a NoSQL database " \
                    "to achieve rapid lookups and schema validity accepting the extra effort required and the need " \
                    "for vertical scaling"
        # When
        design_decision = self.sut.text_to_design_decision(test_text)
        # Then
        self.verify_standard_decision(design_decision)

    def test_sustainable_architectural_design_decisions_example(self):
        # Given
        test_text = "In the context of checking customer’s accounts and signing orders, facing that duties are not " \
                    "adequately segre-gated (SOX 404), we decided to ensure that customers’ accounts are verified " \
                    "by the financial department while the orders are checked and signed by the sales department to " \
                    "achieve proper segregation of duties, accepting that the order processing time is longer "
        # When
        design_decision = self.sut.text_to_design_decision(test_text)
        # Then
        self.assertEqual("checking customer’s accounts and signing orders", design_decision.context.useCase)
        self.assertListEqual(
            [DesignDecisionParser.NonFunctionalConcern("duties are not adequately segre-gated (SOX 404)")],
            design_decision.nonFunctionalConcernsAddressed
        )
        self.assertEqual(DesignDecisionParser.Option(
            "ensure that customers’ accounts are verified by the financial department while the orders are checked "
            "and signed by the sales department"),
            design_decision.chosenOption
        )
        self.assertListEqual(
            [],
            design_decision.neglectedOptions
        )
        self.assertListEqual(
            [DesignDecisionParser.QualityAttribute("proper segregation of duties")],
            design_decision.qualityAttributesAchieved
        )
        self.assertListEqual(
            [DesignDecisionParser.Consequence("the order processing time is longer")],
            design_decision.consequences
        )

    def test_invalid_text_throws_exception(self):
        # Given
        test_text = "not a valid design decision"
        # When, Then
        with self.assertRaises(DesignDecisionParser.InvalidDesignDecisionTextError):
            self.sut.text_to_design_decision(test_text)

    def verify_standard_decision(self, design_decision):
        self.assertEqual("The add users use case", design_decision.context.useCase)
        self.assertListEqual(
            [DesignDecisionParser.NonFunctionalConcern("speed"),
             DesignDecisionParser.NonFunctionalConcern("reliability")],
            design_decision.nonFunctionalConcernsAddressed
        )
        self.assertEqual(DesignDecisionParser.Option("a relational database"), design_decision.chosenOption)
        self.assertListEqual(
            [DesignDecisionParser.Option("an in memory datastore"), DesignDecisionParser.Option("a NoSQL database")],
            design_decision.neglectedOptions
        )
        self.assertListEqual(
            [DesignDecisionParser.QualityAttribute("rapid lookups"),
             DesignDecisionParser.QualityAttribute("schema validity")],
            design_decision.qualityAttributesAchieved
        )
        self.assertListEqual(
            [DesignDecisionParser.Consequence("the extra effort required"),
             DesignDecisionParser.Consequence("the need for vertical scaling")],
            design_decision.consequences
        )

    def test_get_strings_separated_comma(self):
        # Given
        input_text = "Option1, Option2, Option3, Option4"

        # When
        result = self.sut.get_strings_separated(input_text, DesignDecisionParser.Option)

        # Then
        self.assertListEqual([
            DesignDecisionParser.Option("Option1"),
            DesignDecisionParser.Option("Option2"),
            DesignDecisionParser.Option("Option3"),
            DesignDecisionParser.Option("Option4")
        ], result)


    def test_get_strings_separated_and(self):
        # Given
        input_text = "Option1 and Option2 and Option3 and Option4"

        # When
        result = self.sut.get_strings_separated(input_text, DesignDecisionParser.Option)

        # Then
        self.assertListEqual([
            DesignDecisionParser.Option("Option1"),
            DesignDecisionParser.Option("Option2"),
            DesignDecisionParser.Option("Option3"),
            DesignDecisionParser.Option("Option4")
        ], result)

    def test_get_strings_separated_mixed(self):
        # Given
        input_text = "Option1 and Option2, Option3 and Option4"

        # When
        result = self.sut.get_strings_separated(input_text, DesignDecisionParser.Option)

        # Then
        self.assertListEqual([
            DesignDecisionParser.Option("Option1"),
            DesignDecisionParser.Option("Option2"),
            DesignDecisionParser.Option("Option3"),
            DesignDecisionParser.Option("Option4")
        ], result)

    def test_get_strings_separated_no_class_comma(self):
        # Given
        input_text = "Option1, Option2, Option3, Option4"

        # When
        result = self.sut.get_strings_separated_no_class(input_text, DesignDecisionParser.Option)

        # Then
        self.assertListEqual([
            "Option1",
            "Option2",
            "Option3",
            "Option4"
        ], result)

    def test_get_strings_separated_no_class_and(self):
        # Given
        input_text = "Option1 and Option2 and Option3 and Option4"

        # When
        result = self.sut.get_strings_separated_no_class(input_text, DesignDecisionParser.Option)

        # Then
        self.assertListEqual([
            "Option1",
            "Option2",
            "Option3",
            "Option4"
        ], result)

    def test_get_strings_separated_no_class_mixed(self):
        # Given
        input_text = "Option1 and Option2, Option3 and Option4"

        # When
        result = self.sut.get_strings_separated_no_class(input_text, DesignDecisionParser.Option)

        # Then
        self.assertListEqual([
            "Option1",
            "Option2",
            "Option3",
            "Option4"
        ], result)

    def test_text_to_context_invalid(self):

        # Given an invalid context string is passed

        input_text = ""

        # When we attempt to parse this into a string
        # Then we expect an attempt to parse this to fail

        with self.assertRaises(DesignDecisionParser.InvalidDesignDecisionTextError):
            self.sut.text_to_context(input_text)

    def test_text_to_context_valid(self):

        # Given a valid context string is passed

        input_text = "use case and component"

        # When we attempt to parse this into a string

        result = self.sut.text_to_context(input_text)

        # Then we expect an attempt to parse this to pass

        self.assertEqual(DesignDecisionParser.Context("use case", component="component"), result)

    def test_optional_non_functional_concerns(self):

        error_creating = False

        try:
            dd = DesignDecisionParser.DesignDecision(
                context=DesignDecisionParser.Context("use case", component="component"),
                nonFunctionalConcernsAddressed=None,
                chosenOption=DesignDecisionParser.Option("option1"),
                neglectedOptions=[DesignDecisionParser.Option("option2")],
                qualityAttributesAchieved=[DesignDecisionParser.QualityAttribute("quality")],
                consequences=[DesignDecisionParser.Consequence("consequence")],
                inputText=None
            )
        except:
            error_creating = True

        self.assertFalse(error_creating)

    def test_optional_neglected_options(self):
        error_creating = False

        try:
            dd = DesignDecisionParser.DesignDecision(
                context=DesignDecisionParser.Context("use case", component="component"),
                nonFunctionalConcernsAddressed=[DesignDecisionParser.NonFunctionalConcern("nfc")],
                chosenOption=DesignDecisionParser.Option("option1"),
                neglectedOptions=None,
                qualityAttributesAchieved=[DesignDecisionParser.QualityAttribute("quality")],
                consequences=[DesignDecisionParser.Consequence("consequence")],
                inputText=None
            )
        except:
            error_creating = True

        self.assertFalse(error_creating)

    def test_optional_quality_attributes(self):
        error_creating = False

        try:
            dd = DesignDecisionParser.DesignDecision(
                context=DesignDecisionParser.Context("use case", component="component"),
                nonFunctionalConcernsAddressed=[DesignDecisionParser.NonFunctionalConcern("nfc")],
                chosenOption=DesignDecisionParser.Option("option1"),
                neglectedOptions=[DesignDecisionParser.Option("option2")],
                qualityAttributesAchieved=None,
                consequences=[DesignDecisionParser.Consequence("consequence")],
                inputText=None
            )
        except:
            error_creating = True

        self.assertFalse(error_creating)

    def test_optional_consequences(self):
        error_creating = False

        try:
            dd = DesignDecisionParser.DesignDecision(
                context=DesignDecisionParser.Context("use case", component="component"),
                nonFunctionalConcernsAddressed=[DesignDecisionParser.NonFunctionalConcern("nfc")],
                chosenOption=DesignDecisionParser.Option("option1"),
                neglectedOptions=[DesignDecisionParser.Option("option2")],
                qualityAttributesAchieved=[DesignDecisionParser.QualityAttribute("quality")],
                consequences=None,
                inputText=None
            )
        except:
            error_creating = True

        self.assertFalse(error_creating)


if __name__ == '__main__':
    unittest.main()
